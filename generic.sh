###############################################################################
#
# Copyright 2018 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten
# ASTRON Netherlands Institute for Radio Astronomy
# JIVE (Joint Institute for VLBI in Europe)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License. 
#
###############################################################################

# This file contains a collection of convenience functions and definitions.
#
# automatically export to subsequent commands (-a)
set -a
#exits on any error in pipeline, not just the last error
set -o pipefail

# only set variables if we didn't set them before
if [ "${generic_read:-not_set}" = "not_set" ]; then

# display a (colourfull ...) error message. 
#    the script will be terminated immediately
# exit with <errorcode> (default=1)
# usage:  hdl_error <caller> <message> [<errorcode>]
hdl_error() {
    caller=${1:-""}
    msg=${2:-""}
    exitcode=${3:-1}
    if [ -z "${caller}" -o -z "${msg}" ]; then
        echo "usage: hdl_error <caller's name> <message> [<exitcode>]"
        exit 1
    fi
    caller=$(basename ${caller} | tr [a-z] [A-Z])
    echo -n "$(tput setaf 6)$(tput bold)[${caller}] "
    echo -e "$(tput setaf 1)ERROR - ${msg}. $(tput sgr0)"
    # Exit if $NO_EXIT does not exist, else only return
    if [ -z ${NO_EXIT:-""} ]; then exit ${exitcode}; else return 1; fi
}

# Non-exiting version of hdl_error in case we wish to accumulate errors and
# call an exiting hdl_error after displaying accumulated errors.
hdl_error_noexit() {
    caller=${1:-""}
    msg=${2:-""}
    if [ -z "${caller}" -o -z "${msg}" ]; then
        echo "usage: hdl_error <caller's name> <message>"
        exit 1
    fi
    caller=$(basename ${caller} | tr [a-z] [A-Z])
    echo -n "$(tput setaf 6)$(tput bold)[${caller}] "
    echo -e "$(tput setaf 1)ERROR - ${msg}.$(tput sgr0)"
}

hdl_warning() {
    caller=${1:-""}
    msg=${2:-""}
    if [ -z "${caller}" -o -z "${msg}" ]; then
        echo "usage: hdl_warning <caller's name> <message>"
        exit 1
    fi
    caller=$(basename ${caller} | tr [a-z] [A-Z])
    echo -n "$(tput setaf 6)$(tput bold)[${caller}] "
    echo -e "$(tput setaf 3)WARNING - ${msg}.$(tput sgr0)"
    return 0 
}

# usage:  hdl_info <caller> <message>
hdl_info() {
    caller=${1:-""}
    shift
    if [ -z "${caller}" -o -z "$*" ]; then
        echo "usage: hdl_info <scriptname> <msg1> [<msg2> .. <msgN>]"
        exit 1
    fi
    caller=$(basename ${caller} | tr [a-z] [A-Z])
    echo -e "$(tput setaf 6)$(tput bold)[${caller}] $* $(tput sgr0)"
    return 0
}

# usage:
#   hdl_exec <calling script> [OPTS] <command to run>
#  OPTS:
#     [msg=<override defaultmsg>]
#           msg=no => suppress displaying of messages
#                     if command fails, do display the
#                     command that failed
#     [expect=<expected exit code>] (default: 0)
# exits with same exitcode as the command
hdl_exec() {
    # step one: extract calling scriptname, which is $1
    caller=$1; shift
    # anything left is supposedly the command to exec + args
    # prepare the "msg" to display
    msg=
    output=
    expect=0
    # unless someone gave msg="...." as orginal 2nd arg
    #  (and now, since the first "shift", it is 1st)
    for ac ; do
        case ${ac} in 
            output=*)
                # well allrighty then, override default msg 
                output=$(echo "${ac}" | sed 's/^output=//')
                shift
                ;;
            msg=*)
                # well allrighty then, override default msg 
                msg=$(echo "${ac}" | sed 's/^msg=//')
                shift
                ;;
            expect=*)
                expect=$(echo "${ac}" | sed 's/^expect=//')
                shift
                ;;
            * )
                # first non-option argument; stop for loop!
                break
                ;;
        esac
    done
    if [ -z "${msg}" ]; then
        msg="Running \"$*\""
    fi

    # show usr what we're up to
    if [ "${msg}" != "no" ]; then
        hdl_info ${caller} "${msg}"
    fi

    # remember if errexit is switched on because we have to disable it
    exit_on_err=true
    if [ "${SHELLOPTS//*errexit*}" == "$SHELLOPTS" ]; then
        exit_on_err=false
    fi
    set +e

    # and let's actually do it!
    if [ "${output}" = "no" ]; then
      $* >/dev/null 2>/dev/null
    else
      $*
    fi
    exitcode=$?

    # switch errexit on if is was on when we were called (no side effects)
    if [ $exit_on_err ]; then
        set -e
    fi
    echo "exitcode=${exitcode}"
    # Finally check if the exitcode of the command we executed is the expected one.
    if [ "${exitcode}" -ne "${expect}" ]; then
        if [ "${msg}" == "no" ]; then
            echo "****** Failed command ****"
            echo $*
            exit ${exitcode}
        fi
        hdl_error ${caller} "\"${msg}\" failed" $?
    fi
}

# format the date in a specific form
# if changing the format, make sure
# that dateindent has the same length
# again (dateindent used for pretty
# printing multiline stuff without
# having to print the date in every line)
date="/bin/date +'%d %m %Y %T'"
# format    dd mm yyyy HH:MM:ss
dateindent='                   '

#
# Some generic, often used functions
#

# return the current date/time in a
# predefined format - see above
# Use eg as
# echo "`timestamp` Aaargh - Failed to clobber!"
timestamp() {
	echo ${date}
}

# Define function to add directories to a given environment variable
#     args: name_of_env_var new_path [new_path ...]
# Directories are only added when they exist.
pathadd() {
    for new_dir in ${@:2}
    do
        eval dir_to_add=$(echo ${new_dir})
        if [ ! -d ${dir_to_add} ]; then
            echo "WARNING: directory ${dir_to_add} NOT added to $1 because directory doesn't exist!"
        else
            if ! echo ${!1} | grep -E -q "(^|:)$dir_to_add($|:)" ; then
                eval export ${1}=${1:+${!1#:}:}${dir_to_add}
            fi
        fi
    done
    unset dir_to_add new_dir
}

# Mark the fact that we read this file and end the guarded part
generic_read="yes"
fi
