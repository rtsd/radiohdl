# RadioHDL credits

The purpose of this document is to acknowledge the persons, projects and institutes that contribute or contributed to the RadioHDL package.


## Contributors

|Name |Affiliation | Part |
|---|---|---|
|E. Kooistra      | [ASTRON]  | RadioHDL concepts and initial scripts in Python2 |
|D. van der Schuur| [ASTRON]  | Scripts for Modelsim, Quartus and UniBoard1 |
|R. Overeem       | [ASTRON]  | Refactoring of RadioHDL gear in Python3 and bash |
|P. Donker        | [ASTRON]  | Scripts maintenance |
|L. Hiemstra      | [ASTRON]  | Scripts for Vivado and Gemini board |
|M. Baquiran      | [CSIRO]   | Scripts for Vivado and Gemini board |
|W. Poiesz        | [Inspiro] | Initial scripts for Modelsim |
|H. Verkouter     | [JIVE]    | Initial bash scripts for Quartus and UniBoard1 |



## Projects

Initial scripts for Modelsim were developed for the RSP and TBB FPGA boards in the LOFAR stations [LOFAR].

Initial scripts for Quartus and the RadioHDL configuration files were developed for UniBoard1 in APERTIF [1]

Initial scripts for Vivado were developed for the Gemini board in SKA CSP.Low.CBF [2]


## References

[1] "The application of UniBoard as a beam former for APERTIF”, A. Gunst, A. Szomoru, G. Schoonderbeek, E. Kooistra, D. van der Schuur, H. Pepping, Experimental Astronomy, Volume 37, Issue 1, pp 55-67, February 2014, doi:10.1007/s10686-013-9366-x.

[2]:"Gemini FPGA Hardware Platform for the SKA Low Correlator and Beamformer", E. Kooistra, G. A. Hampson, A. W. Gunst, J. D. Bunton, G. W. Schoonderbeek, A. Brown, 32nd URSI GASS, Montreal, 19-26 August 2017


[LOFAR]:http://www.lofar.org/
[ASTRON]:https://www.astron.nl
[JIVE]:https://www.jive.nl/
[Inspiro]:https://www.inspiro.nl
[CSIRO]:https://www.csiro.au/en/Research/Astronomy
