Citation Notice version 1.0

```
This Citation Notice is part of the RadioHDL software suite.

Parties that use ASTRON Software resulting in papers and/or publications are requested to
refer to the DOI(s) that correspond(s) to the version(s) of the ASTRON Software used:

10.5281/zenodo.3631361

Parties that use ASTRON Software for purposes that do not result in publications (e.g.
commercial parties) are asked to inform ASTRON about their use of ASTRON Software, by
sending an email to including the DOIs mentioned above in the message.
```

