#!/usr/bin/env bash
set -e

# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker  (Based on svn regressiontest from Daniel van der Schuur)
# Purpose:
# . normaly this script is executed as a cron job.
#   . Update all git repositories
# Description:
# . Put the following line in Crontab before other tests (by running $sudo crontab -e -u [username]) to run every friday (day 5) at 20:00
#   1 0 * * 5 /home/[username]/git/radiohdl/regressiontest/git_pull_all.sh 2>&1
# 
# ##########################################################################

###############################################################################
# Source environment
# . BEWARE: do not put this (or similar) in your .bashrc:
#   . if [ -z "$PS1" ]; then return; fi. /home/regtest/git/radiohdl/regressiontest/modelsim_regression_test_vhdl_cron.sh
#   This will stop sourcing of .bashrc immediately as a Cron job is not run interactively.
###############################################################################
. "${HOME}"/.bashrc

# read generic functions/definitions
. "${HOME}"/git/radiohdl/generic.sh

echo "update all repositories"

# update args from repository 
hdl_exec "$0" cd "${GIT}"/args || exit
hdl_exec "$0" git reset --hard
hdl_exec "$0" git pull

# update radiohdl from repository (e.g. to get latest config/hdl_buildset_${BUILDSET}.cfg)
hdl_exec "$0" cd "${GIT}"/radiohdl || exit
hdl_exec "$0" git reset --hard
hdl_exec "$0" git pull