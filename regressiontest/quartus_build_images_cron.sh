#!/usr/bin/env bash

# ##########################################################################
# Copyright 2022
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Reinier vd Walle
# Purpose:
# . normaly this script is executed as a cron job.
#   . Update git repositories.
#   . init tools, cleanup and rebuild build environment.
#   . Run build_all_images
#   . Send result by email.
# Description:
# . Put the following line in Crontab (by running $sudo crontab -e -u [username]) to run every friday (day 5) at 20:00
#   0 20 * * 5 /home/[username]/git/radiohdl/regressiontest/quartus_build_images_cron.sh [BUILDFILE] 2>&1
# 
# ##########################################################################

###############################################################################
# Source environment
# . BEWARE: do not put this (or similar) in your .bashrc:
#   . if [ -z "$PS1" ]; then return; fi. /home/regtest/git/radiohdl/regressiontest/modelsim_regression_test_vhdl_cron.sh
#   This will stop sourcing of .bashrc immediately as a Cron job is not run interactively.
###############################################################################
. ${HOME}/.bashrc

# read generic functions/definitions
. "${GIT}"/radiohdl/generic.sh

hdl_exec "$0" cd "${GIT}"/hdl || exit

# init hdl, radiohdl and args
echo "initialize all systems"
. ./init_hdl.sh

export BUILDFILE="${1:-}"
hdl_info "$0" "Build all images in file: ${BUILDFILE}"

LOGFILE="${RADIOHDL_REGTEST_RESULT}/quartus_build_images.log"

echo "update repositories"
# update hdl from repository, 
hdl_exec "$0" git reset --hard
hdl_exec "$0" git pull

# get hash from last test if file excists
git_prev_hash="none"
git_hash_filename="${RADIOHDL_REGTEST_RESULT}"/quartus_previous_git_hash.txt
[[ -f "${git_hash_filename}" ]] && git_prev_hash=$(cat "${git_hash_filename}")
git_active_hash=$(git rev-parse HEAD) 

if [ "${git_active_hash}" == "${git_prev_hash}" ]; then
    hdl_info "$0"  "Skip build NO change in repository"
    # Send nothing changed mail (-n option)
    hdl_info "$0"  "call quartus_build_images_mail"
    hdl_exec "$0" quartus_build_images_mail.py "${LOGFILE}" --nochange 
    exit 1
fi

echo "cleanup old logfile"
# Delete any previous log files
if [ -f "${LOGFILE}" ]; then
    hdl_exec "$0" rm "${LOGFILE}"
fi

echo "cleanup last build"
# Cleanup last build if excists
hdl_exec "$0" rm -rf "${HDL_BUILD_DIR:?}"/*

# Build all IP 
echo "call generate_ip_libs"
hdl_exec "$0" generate_ip_libs unb2b 1>> "${LOGFILE}" 2>&1
hdl_exec "$0" generate_ip_libs unb2c 1>> "${LOGFILE}" 2>&1

# Perform synthesis with output folder $HOME/bitstream and put the output in a log file
echo "call build_all_images"
hdl_exec "$0" build_all_images "${BUILDFILE}" 1>> "${LOGFILE}" 2>&1

# Read the log file en send the result email
echo "call quartus_build_images_mail"
hdl_exec "$0" quartus_build_images_mail.py "${LOGFILE}"

# save hash for next run
echo "${git_active_hash}" > "${git_hash_filename}"
echo "done"
