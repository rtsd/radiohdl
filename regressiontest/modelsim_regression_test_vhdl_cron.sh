#!/usr/bin/env bash

# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker  (Based on svn regressiontest from Daniel van der Schuur)
# Purpose:
# . normaly this script is executed as a cron job.
#   . Update git repositorie, if repositorie not changed skip test.
#   . init tools, cleanup and rebuild build environment.
#   . Run modelsim_regression_test_vhdl.py
#   . Send result by email.
# Description:
# . Put the following line in Crontab (by running $sudo crontab -e -u [username]) to run every friday (day 5) at 20:00
#   0 20 * * 5 /home/[username]/git/radiohdl/regressiontest/modelsim_regression_test_cron.sh /home/[username]/git/radiohdl/regressiontest/modelsim_buildset_list.txt 2>&1
#
# ##########################################################################

###############################################################################
# Source environment
# . BEWARE: do not put this (or similar) in your .bashrc:
#   . if [ -z "$PS1" ]; then return; fi. /home/regtest/git/radiohdl/regressiontest/modelsim_regression_test_vhdl_cron.sh
#   This will stop sourcing of .bashrc immediately as a Cron job is not run interactively.
###############################################################################
. "${HOME}"/.bashrc

# read generic functions/definitions
. "${HOME}"/git/radiohdl/generic.sh

buildset_filename="${1:-}"
echo ".. Do regressiontest for buildsets in file: ${buildset_filename}"

# go to the hdl firmware repository, exit if not excisting
cd "${HOME}"/git/hdl || exit

# init hdl
echo ".. initialize hdl"
. ./init_hdl.sh

# update hdl from repository,
echo ".. Update hdl repository"
git reset --hard
git pull

# get hash from last test if file excists
echo ".. Get previous hash"
git_prev_hash="none"
git_hash_filename="${RADIOHDL_REGTEST_RESULT}"/modelsim_previous_git_hash.txt
[[ -f "${git_hash_filename}" ]] && git_prev_hash=$(cat "${git_hash_filename}")
echo ".. Get active hash"
git_active_hash=$(git rev-parse HEAD)
echo "   - Previous hash is >${git_prev_hash}<"
echo "   - Active hash is   >${git_active_hash}<"
echo ".. Save active hash for next run"
echo "${git_active_hash}" > "${git_hash_filename}"


# get buildsets to check
echo ".. Read buildsets to check from ${buildset_filename}"
# make array with builsets
buildsets=()
while read -r line; do
    # skip empty lines
    [[ -z "$line" ]] && continue
    
    # skip lines starting with #
    [[ "$line" =~ ^# ]] && continue
    
    # get firts word of line
    buildset=$(echo "${line}" | cut -d " " -f1)
    buildsets+=("${buildset}")
done < ${buildset_filename}
echo "   Buildsets to test:"
for buildset in "${buildsets[@]}"; do
    echo "   - ${buildset}"
done

# if not already exist, make output dirs for logging
for buildset in "${buildsets[@]}"; do
    if [ ! -d "${RADIOHDL_REGTEST_RESULT}/${buildset}/modelsim" ]; then
        mkdir -p "${RADIOHDL_REGTEST_RESULT}/${buildset}/modelsim"
    fi
done

# Modelsim test for each line in 'modelsim_buildset_list.txt'
for buildset in "${buildsets[@]}"; do
    echo ".. Do all regressiontests for ${buildset}"

    logfile="${RADIOHDL_REGTEST_RESULT}/${buildset}/modelsim_regressiontest.log"
    
    # if same hash, and not last simulation failed. send mail no change and stop
    if [ "${git_active_hash}" == "${git_prev_hash}" ] && ! grep -Fq "FAILED" "${logfile}"; then
        echo ".. Skip regression test, NO change in repository"
        # Send nothing changed mail (-n option)
        echo ".. Call modelsim_regression_test_vhdl_mail --nochange"
        hdl_exec "$0" modelsim_regression_test_vhdl_mail.py "${buildset}" --nochange  1>> "${logfile}" 2>&1
        continue
    fi

    # Using > will start a new file
    echo "Do all regressiontests for ${buildset}" > "${logfile}"
    
    # Delete tempory modelsim files '/tmp/VSOUT*'
    echo ".. Remove old /tmp/VSOUT* files"
    hdl_exec "$0" rm -f /tmp/VSOUT*  1>> "${logfile}" 2>&1

    # Cleanup last build if exists
    echo ".. Cleanup last build ${buildset}"
    hdl_exec "$0" rm -rf "${HDL_BUILD_DIR:?}"/"${buildset}"  1>> "${logfile}" 2>&1

    # Build all IP
    echo ".. Call compile_altera_simlibs"
    hdl_exec "$0" compile_altera_simlibs "${buildset}" 1>> "${logfile}" 2>&1
    echo ".. Call generate_ip_libs"
    hdl_exec "$0" generate_ip_libs "${buildset}" 1>> "${logfile}" 2>&1

    # re-create it with modelsim_config and quartus_config
    echo ".. Call modelsim_config"
    hdl_exec "$0" modelsim_config "${buildset}" 1>> "${logfile}" 2>&1
    echo ".. Call quartus_config"
    hdl_exec "$0" quartus_config "${buildset}" 1>> "${logfile}" 2>&1

    # Perform the regression test and put the output in a log file
    # -p 4: number of simultaneously test processes, each process needs a license
    echo ".. Call modelsim_regression_test_vhdl"
    hdl_exec "$0" modelsim_regression_test_vhdl.py "${buildset}" -p 2 1>> "${logfile}" 2>&1

    # Read the log file en send the result email
    echo ".. Call modelsim_regression_test_vhdl_mail"
    hdl_exec "$0" modelsim_regression_test_vhdl_mail.py "${buildset}" 1>> "${logfile}" 2>&1
done

echo "All buildsets done"
