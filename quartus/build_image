#!/bin/bash
set -eu
# -------------------------------------------------------------------------- #
#
# Copyright (C) 2022
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# JIVE (Joint Institute for VLBI in Europe) <http://www.jive.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# -------------------------------------------------------------------------- #
#
# Author: Reinier vd Walle
# Purpose: Run all commands in order to compile a Quartus design.
# Description: Executes quartus_config, run_qsys_pro, gen_rom_mmap.py, run_reg,
# run_qcomp and run_rbf to do a full compilation of a quartus design. The bitstream
# files (.sof and.rbf) will be in the corresponding build directory of the design.
# Run this tool with at least the commandline arguments:
#   build_image buildset project_name --rev revision_name
# example:
#   build_image unb2c lofar2_unb2c_sdp_station --rev lofar2_unb2c_sdp_station_full

# read generic functions/definitions
. "${RADIOHDL_GEAR}"/generic.sh

# helper function for command parsing
exit_with_error() {
    hdl_error_noexit "${0}" "$@"
    cat <<@EndOfHelp@
Usage: $(basename "${0}") buildset project [options]
Arguments: buildset     Name of the buildset to create the image for.
           project      Main Project name.

Options: --rev=*        which revision to use.
         --seed=*       which seed(s) to use for fitting (1,2,3,5,8,9).
--> Note: It does not matter where the options are placed: before, in between or after the arguments.
@EndOfHelp@
    exit 1
}

# parse cmdline
POSITIONAL=()
rev=
SEED=1
while [[ $# -gt 0 ]]
do
    case $1 in
        --rev=*)
            rev=${1#*=}
            ;;
        --seed=*)
            SEED=${1#*=}
            ;;
        -*|--*)
            exit_with_error "Unknown option: " "${1}"
            ;;
        *)  POSITIONAL+=("$1")
            ;;
    esac
    shift
done
if [ ${#POSITIONAL[@]} -gt 0 ]; then
    set -- "${POSITIONAL[@]}"
fi

# check the positional parameters
if [ $# -lt 2 ]; then
    exit_with_error "Wrong number of arguments specified."
fi

buildset=$1
project=$2
# read in the configuration based on the user arguments
. "${RADIOHDL_GEAR}"/quartus/set_quartus "${buildset}"

# Run Quartus Config
hdl_exec "$0" quartus_config "${buildset}"


PRJS="${HDL_BUILD_DIR}"
PRJ=
for prj in ${PRJS}
    do
        echo "check if project '${project}' in dir '${prj}'"
        if [ -d "${prj}/${buildset}/quartus/${project}" ]; then
            PRJ=${prj}
        fi
    done
if [ -z "${project}" -o -z "${PRJ}" ]; then
    hdl_error "$0" "Please enter a valid project name"
fi

# check if the quartus project directory is there
quartusdir="${PRJ}/${buildset}/quartus/${project}"
hdl_exec "$0" msg=no test -d "${quartusdir}"

if [ -z "${rev}" ]; then
  project_rev="${project}"
  echo "No project revision passed, defaulting to ${project_rev}"
else
  if [ -f "${quartusdir}/../${rev}/${rev}.qsf" ]; then
    project_rev="${rev}"
    echo "Selecting project revision ${project_rev}"
  else
    hdl_error "$0" "Invalid project revision"
  fi
fi

# Run QSYS
hdl_exec "$0" run_qsys_pro "${buildset}" "${project_rev}"

# Generate ROM MMAP
hdl_exec "$0" gen_rom_mmap.py --avalon -d "${project}" -r "${project_rev}"

# run_reg
hdl_exec "$0" run_reg "${buildset}" "${project_rev}"

# run_qcomp
hdl_exec "$0" run_qcomp "${buildset}" "${project_rev}" --clk=CLK --seed="${SEED}"

# run_rbf
# for user image now done in run_qcomp
# hdl_exec "$0" run_rbf "${buildset}" "${project_rev}"
