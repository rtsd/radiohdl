#!/bin/bash -eu
###############################################################################
#
# Copyright (C) 2014-2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################

# does user wants feedback on what is happening?
if [ "${1:-}" == "--verbose" ]; then
    verbose=$1
    shift
else
    verbose=""
fi

# read generic functions
. ${RADIOHDL_GEAR}/generic.sh

# Buildset settings for selected a target, e.g.: "unb1", or "unb2"
buildset=${1:-}
if [ "${buildset}" = "" ]; then
  hdl_error $0 "Please specify all arguments\nUsage: $0 <buildset>"
fi

export BUILDSET=${buildset}

# Select output directory for the library compilation results
buildset_config_file="${RADIOHDL_CONFIG}/hdl_buildset_${buildset}.cfg"
if [ ! -f $buildset_config_file ]; then
  hdl_error $0 "File '$buildset_config_file' does not exist"
fi
. ${RADIOHDL_GEAR}/set_config_variable $verbose $buildset_config_file synth_tool_name,synth_tool_version,block_design_names

echo "Setting up quartus for: ${buildset}"

# Adopt buildset dependend directories
. ${RADIOHDL_GEAR}/set_config_variable $verbose $buildset_config_file ${SYNTH_TOOL_NAME}_dir

# Adopt quartus installation dependend directories
quartus_config_file="${RADIOHDL_CONFIG}/hdl_tool_quartus.cfg"
. ${RADIOHDL_GEAR}/set_config_variable $verbose $quartus_config_file quartus_rootdir,quartus_rootdir_override,niosdir

# Extend the PATH for quartus
. ${RADIOHDL_GEAR}/set_config_path $verbose $quartus_config_file quartus_paths

# Extend the PATH with the block-design related and user (free defined) paths
for bd_name in $BLOCK_DESIGN_NAMES "user"
do
    . ${RADIOHDL_GEAR}/set_config_path $verbose $quartus_config_file ${bd_name}_paths
done

# Adopt block-design related and user (free defined) environment variables
for bd_name in $BLOCK_DESIGN_NAMES "user"
do
    . ${RADIOHDL_GEAR}/set_config_variable $verbose $quartus_config_file ${bd_name}_environment_variables --is-group --optional
done

# TODO: move to hdl_tool_quartus.cfg : user_environment_variables ???
# HDL_GIT_REVISION (first 32 digits of full 40-digits hash) is used in firmware
# HDL_GIT_REVISION_SHORT (first 9 digits of full hash) is used in file-naming
if [[ -n "${HDL_WORK}" ]]; then
    git_rev=`cd $HDL_WORK; git rev-parse HEAD; cd -`
    export HDL_GIT_REVISION=${git_rev}
    export HDL_GIT_REVISION_SHORT=${git_rev:0:9}
    #export HDL_GIT_REVISION=1010101
    #export HDL_GIT_REVISION_SHORT=101
    echo "set GIT revision to '${HDL_GIT_REVISION}' and as short '${HDL_GIT_REVISION_SHORT}'"
else
    echo "GIT revision not set (HDL_WORK not available), run first '. ./init_hdl.sh' in hdl dir"
fi
export UNB_COMPILE_STAMPS=1

unset buildset_config_file quartus_config_file bd_name verbose git_rev
