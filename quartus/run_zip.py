#!/usr/bin/python3

from argparse import ArgumentParser
import zlib

def main():
    data_file_name = args.file
    zdata_file_name = data_file_name + '.z'

    with open(data_file_name, 'r') as fd:
        data = fd.read().encode('utf-8')

    print(f"{len(data)} bytes to zip")
    zdata = zlib.compress(data, -1)

    with open(zdata_file_name, 'bw') as fd:
        response = fd.write(zdata)
        print(f"{response} bytes written to zipped file")

if __name__ == "__main__":
    parser = ArgumentParser(description='Run Zip using zlib')
    parser.add_argument('-f', '--file', required=True, help='File to zip')
    parser.add_argument('-v', '--verbosity', default='INFO', help="stdout log level can be [ERROR | WARNING | INFO | DEBUG]")
    args = parser.parse_args()

    main()
