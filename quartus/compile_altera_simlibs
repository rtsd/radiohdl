#!/bin/bash -eu
###############################################################################
#
# Copyright (C) 2014-2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################

# Purpose: Compile the Altera simulation libraries
# Description:
#   This script is equivalent to manually running the Quartus GUI tools/Launch simulation library compiler.
#   However it is needed to use this script, because then the Altera libraries can be compiled with the 'vlib -type directory' option.
#   Default 'vlib' compiles all components into a single binary but to be able to use 'mk all' it is necessary that each component
#   is compiled into a seperate binary.
#   Therefore this script uses 'sed' to replace 'vlib' by 'vlib -type directory' in the created Modelsim .do file and then
#   it then runs the .do file. This needs to be done for all families (arria10) and all HDL (vhdl and verilog).
#
#   > run_altera_simlib_comp unb2
#
#   Then use 'sudo' to move the directory to the protected central project directory that is set by $MODEL_TECH_ALTERA_LIB.
#

echo "run compile_altera_simlibs"

# read generic functions/definitions
. ${RADIOHDL_GEAR}/generic.sh

# check the positional parameters
if [ $# -ne 1 ]; then
    hdl_error $0 "Syntax: $0 buildset"
fi

buildset=$1
# read in the configuration based on the user arguments
. ${RADIOHDL_GEAR}/quartus/set_quartus ${buildset}
. ${RADIOHDL_GEAR}/modelsim/set_modelsim ${buildset}

# Select output directory for the library compilation results
buildset_config_file="${RADIOHDL_CONFIG}/hdl_buildset_${buildset}.cfg"
. ${RADIOHDL_GEAR}/set_config_variable $buildset_config_file synth_tool_name,synth_tool_version
OUTPUT_DIR=${HDL_BUILD_DIR}/${SYNTH_TOOL_NAME}/${SYNTH_TOOL_VERSION}
mkdir -p ${OUTPUT_DIR}
mkdir -p ${HDL_BUILD_DIR}/${buildset}

# Select FPGA device family for the library compilation
. ${RADIOHDL_GEAR}/set_config_variable $buildset_config_file family_names

echo ""
echo "Create Altera libraries for simulation with the following settings:"
echo "    - Buildset target     : ${buildset}"
echo "    - Output directory    : ${OUTPUT_DIR}"
echo "    - FPGA device families: ${FAMILY_NAMES}"
echo ""

for FAMILY in $FAMILY_NAMES
do
    # 2) Create Modelsim .do file for compiling the Altera simulation libraries
    # . verilog
    quartus_sh --simlib_comp -family ${FAMILY} \
                             -tool modelsim \
                             -tool_path $MODELSIM_DIR/$MODELSIM_PLATFORM \
                             -language verilog \
                             -directory ${OUTPUT_DIR} \
                             -log ${OUTPUT_DIR}/${FAMILY}_verilog.log \
                             -cmd_file ${FAMILY}_verilog.do \
                             -gen_only \
                             -suppress_messages
    # . vhdl
    quartus_sh --simlib_comp -family ${FAMILY} \
                             -tool modelsim \
                             -tool_path $MODELSIM_DIR/$MODELSIM_PLATFORM \
                             -language vhdl \
                             -directory ${OUTPUT_DIR} \
                             -log ${OUTPUT_DIR}/${FAMILY}_vhdl.log \
                             -cmd_file ${FAMILY}_vhdl.do \
                             -gen_only \
                             -suppress_messages

    # Go to the output directory
    cd ${OUTPUT_DIR}

    # 3) Now use sed to replace 'vlib' by 'vlib -type directory'
    # TODO: unb1 dependancy!!!
    if [ "${buildset}" != "unb1" ]; then
      sed -i 's/vlib/vlib -type directory/g' ${FAMILY}_verilog.do
      sed -i 's/vlib/vlib -type directory/g' ${FAMILY}_vhdl.do
    fi

    # 4) Compile the Altera libraries with Modelsim
    cli_modelsim ${buildset} ${FAMILY}_verilog.do
    cli_modelsim ${buildset} ${FAMILY}_vhdl.do

    # 5) Leave files for modelsim_config
    libraryfile=${HDL_BUILD_DIR}/${buildset}/hdl_libraries_ip_${FAMILY}.txt
    rm -f ${libraryfile}
    grep vmap ${OUTPUT_DIR}/${FAMILY}_*.do | sort | \
         sed "s?${OUTPUT_DIR}?\$MODEL_TECH_ALTERA_LIB?g" | while read vmap label location
    do
        echo "$label = $location" | sed "s/\"//g" >> ${libraryfile}
    done
    echo "Created ${libraryfile} for modelsim_config"
done

# 5) Show all errors and warnings that occured during the comiplation
echo
echo "The following warnings and error occured during the compilation:"
grep -i -e Warning: -e Error: -B1 ${OUTPUT_DIR}/transcript
