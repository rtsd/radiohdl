# Used by modelsim_config.py to create library paths in the HDL library project files
# VHDL
unisim = $MODEL_TECH_XILINX_LIB/vhdl/unisim
unimacro = $MODEL_TECH_XILINX_LIB/vhdl/unimacro
simprim = $MODEL_TECH_XILINX_LIB/vhdl/simprim
xilinxcorelib = $MODEL_TECH_XILINX_LIB/vhdl/XilinxCoreLib
secureip = $MODEL_TECH_XILINX_LIB/vhdl/secureip
aim = $MODEL_TECH_XILINX_LIB/vhdl/abel/aim
pls = $MODEL_TECH_XILINX_LIB/vhdl/abel/pls
cpld = $MODEL_TECH_XILINX_LIB/vhdl/cpld

# Verilog
unisims_ver = $MODEL_TECH_XILINX_LIB/verilog/unisims_ver
unimacro_ver = $MODEL_TECH_XILINX_LIB/verilog/unimacro_ver
uni9000_ver = $MODEL_TECH_XILINX_LIB/verilog/uni9000_ver
simprims_ver = $MODEL_TECH_XILINX_LIB/verilog/simprims_ver
xilinxcorelib_ver = $MODEL_TECH_XILINX_LIB/verilog/XilinxCoreLib_ver
aim_ver = $MODEL_TECH_XILINX_LIB/verilog/aim_ver
cpld_ver = $MODEL_TECH_XILINX_LIB/verilog/cpld_ver
