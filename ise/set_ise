#!/bin/bash
###############################################################################
#
# Copyright (C) 2016
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Purpose: Map command line argument to a tool version
# Description:
#   By using command line arguments like "rsp" to select the tool
#   version it is easier to manage combinations of tool versions.

# General tool and project settings
# - use '. <script>.sh' to have the settings apply in this shell, otherwise they get lost when <script>.sh returns

# Select target
TOOLSET=${1:-}
echo "Make tool settings for RadioHDL with :"
echo "  - Tool setting: ${TOOLSET}"
echo "  - Site setting: ${SITE-}"

# Select tool version
if [ "${SITE-}" = "USN" ]; then
  echo "Optionally use site dependent toolset for ${TOOLSET}"

else # default site
  if [ "${TOOLSET}" = "rsp" ]; then
    . ${RADIOHDL_GEAR}/ise/ise_version.sh 10.1.03
  else
    echo "error: unknown tool setting: ${TOOLSET} (choose 'rsp')"
    exit 1
  fi
fi

# Tool settings
. ${RADIOHDL_GEAR}/ise/ise_generic.sh
