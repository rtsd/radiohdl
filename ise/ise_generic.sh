#!/bin/bash
###############################################################################
#
# Copyright (C) 2014
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

echo "Run ise_generic.sh"

# Derive generic ISE tool version related paths from $ISE_DIR that gets defined in ise_version.sh

# Add to the $PATH, only once to avoid double entries
pathadd ${ISE_DIR}/bin/lin

#WARNING:Place:957 - Placer has detected that XIL_PLACE_ALLOW_LOCAL_BUFG_ROUTING has been set. This environment variable
#   has been deprecated. An ERROR in clock placement rules can be demoted to a WARNING by using the CLOCK_DEDICATED_ROUTE
#   constraint on a specific component pin in the .ucf file.

#set XIL_PLACE_ALLOW_LOCAL_BUFG_ROUTING=1

# ERROR:Pack:1653 - At least one timing constraint is impossible to meet because
#    component delays alone exceed the constraint. A timing constraint summary
#    below shows the failing constraints (preceded with an Asterisk (*)). Please
#    use the Timing Analyzer (GUI) or TRCE (command line) with the Mapped NCD and
#    PCF files to identify which constraints and paths are failing because of the
#    component delays alone. If the failing path(s) is mapped to Xilinx components
#    as expected, consider relaxing the constraint. If it is not mapped to
#    components as expected, re-evaluate your HDL and how synthesis is optimizing
#    the path. To allow the tools to bypass this error, set the environment
#    variable XIL_TIMING_ALLOW_IMPOSSIBLE to 1.

set XIL_TIMING_ALLOW_IMPOSSIBLE=1

