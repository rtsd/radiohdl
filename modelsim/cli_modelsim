#!/bin/bash -eu
###############################################################################
#
# Copyright (C) 2014-2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# General tool and project settings
# - use '. <script>.sh' to have the settings apply in this shell, otherwise they get lost when <script>.sh returns

# read generic functions/definitions
# first clear generic_read flag
generic_read=
. ${RADIOHDL_GEAR}/generic.sh

# helper function for command parsing
exit_with_error() {
    hdl_error_noexit $0 "$@"
    cat <<@EndOfHelp@
Usage: $(basename $0) buildset [.do-file]
Arguments: buildset     Name of the buildset to start modelsim for.
           project      Command file to use.
@EndOfHelp@
    exit 1
}

# parse cmdline
POSITIONAL=()
while [[ $# -gt 0 ]]
do
    case $1 in
        -*|--*)
            exit_with_error "Unknown option: "$1
            ;;
        *)  POSITIONAL+=("$1")
            ;;
    esac
    shift
done
if [ ${#POSITIONAL[@]} -gt 0 ]; then
    set -- "${POSITIONAL[@]}"
fi

# check the positional parameters
if [ $# -ne 1 ] && [ $# -ne 2 ]; then
    exit_with_error "Wrong number of arguments specified."
fi
buildset=$1
do_file=${2:-}

. ${RADIOHDL_GEAR}/modelsim/set_modelsim ${buildset}

if [ -z "${do_file}" ]; then
    # Start the Modelsim command line with commands.do
    dofile="${RADIOHDL_GEAR}/modelsim/commands.do"
else
    # Start the Modelsim command line with user passed .do file
    # Typically the do-file should start with 'do ${RADIOHDL_GEAR}/modelsim/commands.do' and end with 'quit -f'.
    dofile="${do_file}"
fi

# for testing without docker
hdl_exec $0 \
        $MODELSIM_DIR/$MODELSIM_PLATFORM/vsim -c -do ${dofile}
exit 0
# end for testing without docker

# check if docker image exists
if [[ "$(docker images -q "questasim:${SIM_TOOL_VERSION}" 2> /dev/null)" == "" ]]; then
    echo "docker image 'questasim:${SIM_TOOL_VERSION}' does not exists"
    # run modelsim from /home/software dir
    hdl_exec $0 \
        $MODELSIM_DIR/$MODELSIM_PLATFORM/vsim -c -do ${dofile}
else
    hdl_exec $0 \
        docker run -u $(id -u $USER)":"$(id -g $USER) \
                   --net=host \
                   --env="DISPLAY" \
                   --volume="$HOME/.Xauthority:/root/.Xauthority:ro" \
                   --volume="$HOME/git/:$HOME/git" \
                   --cap-add=SYS_PTRACE \
                   --security-opt seccomp=unconfined \
                   -e "HDL_BUILD_DIR=${HDL_BUILD_DIR}" \
                   -e "BUILDSET=${buildset}" \
                   -e "RADIOHDL_GEAR=${RADIOHDL_GEAR}" \
                   -e "LM_LICENSE_FILE=${LM_LICENSE_FILE}" \
                   --rm \
                   -it \
                   "questasim:${SIM_TOOL_VERSION}" vsim -c -do "${RADIOHDL_GEAR}/modelsim/commands.do"
fi