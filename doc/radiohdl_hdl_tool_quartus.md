# RadioHDL tool settings for Quartus

---
#### Document history:
|Revision|Date|Author|Affiliation|Modification|
|:---|:---|:---|:---|:---|
|1.0|27 jan 2020|E. Kooistra|ASTRON |Created.|

---
#### Contents:
1   Introduction  
1.1 Preconditions  
1.2 Overview  
2   Appendix:configuration files  
2.1 hdl_tool_quartus.cfg  
2.2 hdllib.cfg keys for Quartus  
3   Appendix: hdl_tool_quartus.cfg

---
#### Terminology:
- FPGA = Field Programmable Gate Array
- HDL = Hardware Description Language
- IP = Intellectual Property
- QIP = Quartus Intellectual Property (file)
- QPF = Quartus Project File
- QSF = Quartus Setting File

---  
#### References:

[1] radiohdl_user_guide.md  
[2] radiohdl_hdl_buildset_uniboard1.md
[3] radiohdl_hdl_library_key_descriptions.md  

---  
## 1 Introduction

The RadioHDL package consists of a set of scripts that interpret configuration files and that setup or run other tools for HDL code development. This document describes how to use the RadioHDL command line tools [1] with the vendor tools from Intel/Altera.

### 1.1 Preconditions

Install a version of Quartus in ```$ALTERA_DIR/<version id>``` (version 11.1 is used for UniBoard1, but for newer boards newer Intel/.Altera tool versions have been used too).

Setup the RadioHDL environment as explained in [1]. The RadioHDL for Quartus is configured using a ```hdl_tool_<tool_name>.cfg``` configuration = hdl_tool_quartus.cfg file, where the tool_name = quartus is defined in a ```hdl_buildset_<buildset_name>.cfg``` configuration file. The hdl_tool_quartus.cfg is described in appendix 2.

### 1.2 Overview

The following RadioHDL tools are about preparing intermediate targets like project files and compiled or pre-generated IP:

* **compile_altera_simlibs** to compile fixed, standard Altera component libraries for simulation with Modelsim. For example PLL, block RAM, FIFO components.
* **generate_ip_libs** to pre-generate customized, parameterizable IP components for simulation and synthesis, using Altera MegaWizard or Altera QSYS. For example a 10GbE MAC, a DDR4 PHY controller. The custom IP is generated for HDL libraries that have a [generate_ip_libs] section with one or more ```<ip_tool>_ip_files``` keys. The supported IP tools are defined at the ip_tools key in the hdl_tool_quartus.cfg
* **quartus_config** to create Quartus project files (qpf, qsf, qip) for all HDL libraries with a hdllib.cfg. All HDL libraries withsource code that can be synthesized will get a Quartus qip file. The HDL libraries with top level design will also get a Quartus qfp and qsf file.
* **run_sopc** to generate a Altera SOPC memory mapped (MM) bus with NiosII, for an HDL library that has an sopc file
* **run_qsys** to generate a Altera QSYS memory mapped (MM) bus with NiosII, for an HDL library that has an qsys file

Internally **run_sopc** and **run_qsys** can also build the software that runs on the NiosII softcore processor. For this they use **run_app**, which calls **run_bsp** and **run_reg**. The **run_reg** calls **run_mif**.

The following tools are about synthesis and to build an image that can run on an FPGA:

* **run_quartus** to start Altera Quartus GUI for synthesis of an HDL library with a top level design
* **run_qcomp** to synthesize a top level design from the command line. Use option --seed to rerun the synthesis for different seeds, which can make a difference to achieve timing closure.
* **run_rbf** to create an Altera FPGA rbf file from an FPGA sof file, to boot the FPGA from flash

For an example of how to use these RadioHDL scripts please see how they are used for HDL firmware development for UniBoard1 [2].

For hdllib.cfg configuration file examples that are used by **generate_ip_libs** and **quartus_config** please see [3].

---
## 2 Appendix: configuration files

### 2.1 hdl_tool_quartus.cfg

This appendix describes the keys that are used in the hdl_tool_quartus configuration file that is located in $RADIOHDL_GEAR/config.

***quartus_rootdir***, ***quartus_rootdir_override*** and ***niosdir***  
Quartus needs these three environment variables in order to operate properly. Note that you can refer to environment variables that you have defined in your buildset file.

***quartus_paths***  
List of paths that are needed to run Quartus programs. Sometimes paths differ slightly between several versions of a tool. Just mention all paths here because paths that do not exist (for your ```<buildset>``` version) are not added to your PATH variable.

#### 2.1.1 [```<block_design_name>```]
For the tools you mentioned in the key 'block_design_names' of your hdl_buildset file you can define two keys that are automatically recognized by RadioHDL. These keys are:

- ```<block_design_name>_paths```: the paths to add to your PATH variable.
- ```<block_design_name>_environment_variables```: the environment variables this tool need. Note that the name of environment variable and de value of the environment variable are separated by a space.

#### 2.1.2 [ip generation]
This section defines the tools and their options that will be used for the generation of the IP libraries. Currently the generate_ip_libs executable from RadioHDL supports three generation tools: qmegawiz, qsys-generate and quartus_sh. For each of these tools you can define a key ```<block_design_name>_default_options``` that contains the default options to add to the commandline when running this tool. Options that are lib-specific are defined in the hdllib.cfg file. In those files the key ```<block_design_name>_extra_options``` can be used to add other flags/arguments to the IP compilation.

#### 2.1.3 [user settings]
Finally you can define paths and environment variables that are specific for you own need. The keys user_paths and user_environment_variables are recognized by RadioHDL.


### 2.2 hdllib.cfg keys for Quartus

This appendix describes the keys that are used in the [quartus_project_files] section of the hdllib.cfg configuration file. Please see [2] and [3] for hdllib.cfg configuration file examples that use these Quartus specific keys.

***quartus_copy_files***  
Copy listed all directories and files for synthesis with Quartus, used when tool_name_synth = quartus in ```hdl_buildset_<buildset_name>.cfg```. Can be used to e.g. copy SOPC or QSYS file from the hdllib.cfg location directory to the build directory where the Quartus project file is and that is where the ```run_*``` bash commands expect them to be.

***synth_top_level_entity***  
When this key exists then a Quartus project file (QPF) and Quartus settings file (QSF) will be created for this HDL library. If this key does not exist then no QPF and QSF are created. The 'synth_top_level_entity' key specifies the top level entity in the HDL library that will be the top level for synthesis. If the key value is empty '' then the 'hdl_lib_name' is taken as top level entity name. The 'synth_top_level_entity' key in a library indicates that the library is a design library that can be synthesized to yield an FPGA image. A design library should not depend on another design library with a 'synth_top_level_entity' key, because then there may occur conflicting or double design constraints for Quartus.

* Created QPF:
  - It only states that there is one revision that has the name of the 'synth_top_level_entity'. The Quartus scheme for revisions is not used. Instead the RadioHDL scheme of defining design revisions as separate HDL libraries is used [1].
* Created QSF:
  - Defines the top level entity name using 'synth_top_level_entity'
  - It sources the files listed by the 'quartus_qsf_files' key, this is typically a board qsf that defines settings that are common to all designs that target that board, eg. unb1_board.qsf.
  - It sources all library QIP files ```<lib_name>_lib.qip``` that are needed by the design. The library QIP files are sourced in dependency order so that the top level design ```<lib_name>_lib.qip``` is sourced last. In this way the top level design constraints are at the end.
* Created ```<lib_name>_lib.qip``` files:
The ```<lib_name>_lib.qip``` files are created for each library using the following keys in this order:
  - hdl_lib_uses_synth   -- used for all HDL libraries
  - quartus_vhdl_files   -- used for IP libraries that have different HDL file for sim and for synth (typically not needed for most IP)
  - quartus_qip_files    -- used for IP libraries (constraints for the IP), top level design libraries (SOPC or QSYS MMM-bus, e.g. sopc_unb1_minimal.qip)
  - quartus_tcl_files    -- used for top level design libraries (pinning definitions, e.g. unb1_minimal_pins.tcl)
  - quartus_sdc_files    -- used for top level design libraries (timing constraints, e.g. unb1_board.sdc)

***quartus_qsf_files*** (see also 'synth_top_level_entity' description)  
One or more .qsf files that need to be included in the HDL library qsf file for Quartus synthesis of a 'synth_top_level_entity' VHDL file.
        
***quartus_vhdl_files*** (see also 'synth_top_level_entity' description)  
One or more .vhdl files that need to be included in the HDL library qip file for Quartus synthesis. These are VHDL files that must not be simulated, so they are not listed at the 'synth_files' key. This can typically occur for technology IP libraries where e.g. a .vhd file is used for synthesis and a .vho file for simulation like in the tse_sqmii_lvds HDL library.

***quartus_qip_files***  (see also 'synth_top_level_entity' description)  
One or more .qip files that need to be included in the HDL library qip file for Quartus synthesis.
  
***quartus_tcl_files***  (see also 'synth_top_level_entity' description)  
One or more .tcl files that need to be included in the HDL library qip file for Quartus synthesis.

***quartus_sdc_files***  (see also 'synth_top_level_entity' description)  
One or more .sdc files that need to be included in the HDL library qip file for Quartus synthesis.

---
## 3 Appendix: hdl_tool_quartus.cfg

Tool configuration file for Intel/Altera Quartus from $RADIOHDL_GEAR/config.
```
# configuration file for defining the quartus installation on this system
quartus_rootdir          = ${QUARTUS_DIR}/quartus
quartus_rootdir_override = ${QUARTUS_DIR}/quartus
niosdir                  = ${QUARTUS_DIR}/nios2eds

# extension to the PATH variable
quartus_paths =
    <quartus_rootdir>/bin
    <niosdir>/bin
    <niosdir>/bin/gnu/H-i686-pc-linux-gnu/bin
    <niosdir>/bin/gnu/H-x86_64-pc-linux-gnu/bin
    <niosdir>/sdk2/bin

[sopc]
sopc_paths = 
    <quartus_rootdir>/sopc_builder/bin
sopc_environment_variables = 
    sopc_kit_nios2      <niosdir>

[qsys]
qsys_paths = 
    <quartus_rootdir>/../qsys/bin

[ip generation]
ip_tools                      = qmegawiz qsys-generate quartus_sh
qmegawiz_default_options      = -silent
qsys-generate_default_options = --synthesis=VHDL --simulation=VHDL --allow-mixed-language-simulation --block-symbol-file
quartus_sh_default_options    = 

[user settings]
user_paths =
user_environment_variables = 
    altera_hw_tcl_keep_temp_files 1

#unb_compile_stamps 1
```


