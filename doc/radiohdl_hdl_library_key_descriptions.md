# RadioHDL library keys

---
#### Document history:
|Revision|Date|Author|Affiliation|Modification|
|:---|:---|:---|:---|:---|
|1.0|27 jan 2020|E. Kooistra|ASTRON |Created, using text from hdltool_readme.txt..|

---
#### Contents:
1 Introduction  
2 General hdllib,cfg keys  
3 Examples of hdllib.cfg configuration files  
3.1 Example for a simple HDL library  
3.2 Example for a HDL library that uses copies files
3.3 Example for a technology wrapper HDL library  
3.4 Example for a board HDL library  
3.5 Example for a board HDL top level design library  
3.6 Example for a board HDL top level revision design library  
3.7 Example for a HDL library for pre-generating and simulating IP

---
#### References:
[1] radiohdl_user_guide.md  
[2] radiohdl_hdl_buildset_key_descriptions.md  
[3] radiohdl_with_modelsim.md  
[4] radiohdl_with_quartus.md  
[5] radiohdl_hdl_buildset_uniboard1.md

---  
## 1 Introduction
Each HDL library inRadioHDL has a hdllib.cfg file [1]. This document describes the keys that are used in the hdllib.cfg configuration files.

The hdllib.cfg can be divided into sections to group the keys that are used for a specific target. The sections headers are identified by a [section_name]. The first part of the hdllib.cfg has no section header and these general keys are available for all target scripts. The keys within in a section are used by a corresponding target script, for example:

* [modelsim_project_file] used by **modelsim_config**, described in [3]
* [quartus_project_file] used by **quartus_config**, described in [4]

A target script can use keys from more than one section. Future target scripts will have their own [section name] header in the hdllib.cfg. In this way the hdllib.cfg remains more organised and the keys per target are independent.

Keys in the hdllib.cfg can use key values from the ```hdl_buildset_<buildset_name>.cfg``` configuration file [2].

---  
## 2 General hdllib.cfg keys
The general hdllib keys are described here.

***hdl_lib_name***  
The name of the HDL library, e.g. unb1_minimal.

***hdl_library_clause_name***  
The name of the HDL library as it is used in the VHDL LIBRARY clause, e.g. unb1_minimal_lib.

***hdl_lib_uses_synth***  (see also the other ```hdl_lib_include_*``` descriptions)  
List of HDL library names that are used in this HDL library for the 'synth_files', only the libraries that appear in VHDL LIBRARY clauses need to be mentioned, all lower level libraries are found automatically. The following libraries have to be declared at the 'hdl_lib_uses_synth' key:

- Libraries with packages that are used
- Libraries with components that are instantiated as VHDL entities

Libraries that are instantiated as VHDL components can be specified at the 'hdl_lib_uses_synth' key, but instead it may also be specified at the 'hdl_lib_uses_ip' key. If there are different source variants of the component and if these source libraries can be missing in the 'lib_root_dirs' tree, then the library must be specified at the 'hdl_lib_uses_ip' key.

***hdl_lib_uses_ip***  (See also the other ```hdl_lib_include_*``` descriptions)  
The 'hdl_lib_uses_ip' typically defines IP libraries that have multiple variants even within a specific technology (as specified by hdl_buildset key 'technology_names'). However typically only one technology variant of the IP is used in a design. The 'hdl_lib_include_ip' key therefore defines the library that must be included in the list of library dependencies that are derived from 'hdl_lib_uses_ip'. Hence the 'hdl_lib_uses_ip' key defines the multiple choice IP libraries that are available in this library and the 'hdl_lib_include_ip' select which one (or more) are used by a higher level component (design). For technology libraries with only one IP library variant the IP libraries should be listed at the 'hdl_lib_uses_synth' key or at both the 'hdl_lib_uses_ip' and 'hdl_lib_include_ip' key. If a multiple choice IP library can be included always, then it may also be specified at the 'hdl_lib_uses_synth'.
Typically present, but unused IP is no problem. However for synthesis the constraint files of unused IP can cause problems. Therefore then use 'hdl_lib_include_ip' to only include this IP library from the IP variants in 'hdl_lib_uses_ip'. An example is to only include ip_stratixiv_ddr3_uphy_4g_800_master in unb1_ddr3 / io_ddr / tech_ddr by setting hdl_lib_include_ip = ip_stratixiv_ddr3_uphy_4g_800_master in the hdllib.cfg of unb1_ddr3. Another example is ip_stratixiv_tse_sgmii_lvds for tech_tse which is included by the board specific library unb1_board to avoid that the other ip_stratixiv_tse_sgmii_gx variant is also included when it is not actually used. This example also shows that a 'hdl_lib_include_ip' can also occur at some intermediate hierarchical component level in a design. The advantage is that the include of ip_stratixiv_tse_sgmii_lvds in the unb1_board hdlib.cfg now automatically applies to all designs that instantiate unb1_board. The exclusion can only be done when the component is instantiated as a VHDL component and not as a VHDL entity. Therefore the exclusion is done at the IP level, because the IP is instantiated as component. Hence the exclusion works because for a component instance that is not used, only the component declaration (in the component package) needs to be known by the tools. Hence the exclusion makes use of the same VHDL component mechanism as the technology independence.The exclusion is only done for synthesis, so not for simulation. The reason is that for simulation it is oke to keep the library included.
The difference between this 'hdl_lib_uses_ip' key and the 'hdl_lib_technology' key is that the HDL libraries with 'hdl_lib_technology' key value that does not match the specified technologies are not build. Whereas HDL libraries that are excluded via the combination of 'hdl_lib_include_ip' and 'hdl_lib_uses_ip' are still created in the build directory, but they are not used for that HDL library so they are excluded dynamically. 

***hdl_lib_uses_sim***  (see also the other ```hdl_lib_include_*``` descriptions)  
List of HDL library names that are used in this HDL library for the 'test_bench_files', only the libraries that appear in VHDL LIBRARY clauses need to be mentioned, all lower level libraries are found automatically.
The 'hdl_lib_uses_synth' and 'hdl_lib_uses_ip' keys and 'hdl_lib_uses_sim' key separate the dependencies due to the 'synth_files' from the extra dependencies that come from the 'test_bench_files'. Quartus can exit with error if IP is included in the 'hdl_lib_uses_ip' list of libraries, but not actually used in the design, e.g. due to an sdc constraints file that is then sourced but that cannot find some IP signals. Having a seperate 'hdl_lib_uses_ip' and 'hdl_lib_uses_sim' key solves this issue, by avoiding that libraries that are only needed for test bench simulation get included in the list for synthesis. Often the 'test_bench_files' do not depend on other libraries then those that are already mentioned at the 'hdl_lib_uses_synth' key, so then the 'hdl_lib_uses_sim' remains empty.

***hdl_lib_include_ip***  (see also the ```hdl_lib_include_*``` descriptions)  
The ```hdl_lib_uses_*``` keys identify which libraries are available for that particular HDL library. For simulation they are all included. The 'hdl_lib_include_ip' identifies which IP libraries from 'hdl_lib_uses_ip' will actually be included for synthesis. The 'hdl_lib_include_ip' typically appears in another higher layer HDL library. IP libraries can be included in the following ways:

- by listing the IP library name at the 'hdl_lib_uses_synth' key, then it is always included
- by listing the IP library name at the 'hdl_lib_uses_ip' key, and including it explicitly with the 'hdl_lib_include_ip' key.

The 'hdl_lib_include_ip' is typically set at:

- the design library that actually uses that IP library, this then has to be done per design revision.
- for IP in unb*_board that is used in all designs it is set in these unb*_board libraries so that it is then automatically included for all designs that use the unb*_board library (i.e. via ctrl_unb*_board.vhd).
- Note that specifying an IP library at the 'hdl_lib_uses_ip' key and then including it via 'hdl_lib_include_ip' in the same hdllib.cfg, is equivalent to specifying the IP library at the 'hdl_lib_uses_synth' key.

***hdl_lib_disclose_library_clause_names***  (see also the ```hdl_lib_include_*``` descriptions)  
If a component from a library is instantiated as a VHDL component (instead of as an VHDL entity) then that means that this library may be unavailable and then it has to be listed as a pair of lib_name and library_clause_name at this 'hdl_lib_disclose_library_clause_names' key. For components that are instantiated as components the actual source library may have been removed (via the 'hdl_lib_technology' key) or it may even not be present at all. The library clause name of instantiated components is used in the VHDL code at the LIBRARY statement in e.g. a ```tech_*.vhd``` file to ensure default component binding in simulation. The 'hdl_lib_disclose_library_clause_names' key is then used in the hdllib.cfg file of that (technology) wrapper library to disclose the library clause name of the component library that is listed at the ```hdl_lib_uses_*``` key. The disclosed library will be available as an empty library, to avoid a missing library error in Modelsim.

***hdl_lib_technology***  
The IP technology that this library is using or targets, e.g. ip_stratixiv for UniBoard1, ip_arria10 for UniBoard2. For generic HDL libraries use empty ''. For simulating systems with multiple FPGA technologies it is also possible to list multiple IP technology names.

***synth_files***  
Lists all HDL files that are needed for synthesis. For Modelsim they need to be in compile order and they are placed in the 'synth_files' project folder. For Quartus synthesis these files get included in the HDL library qip file. Both Verilog and VHDL files are supported.

***test_bench_files***  
List all HDL files that are needed only for simulation. These are typically test bench files, but also HDL models. For Modelsim they need to be in compile order and they are placed in the 'test_bench_files' project folder. Both Verilog and VHDL files are supported. A synthesis tool should ignore this key.

***regression_test_vhdl***  
List of pure VHDL testbenches that need to be included in the regression simulation test. For Modelsim this key is used by modelsim_regression_test_vhdl.py to simulate all testbenches and report their result in a log. The VDHL test benches must be self-checking and self-stopping.

### 2.1 [section_name]
***```<tool_name>_copy_files```***  
The copy_files key can copy one file or a directory. The first value denotes the source file or directory and the second value denotes the destination directory. The paths may use environment variables. The file path or directory can be an absolute path or a relative path. The relative path can be from hdllib.cfg location or from the build dir location. Whether the source directory is the hdllib.cfg location or the build location depends on the ```<tool_name>```. For modelsim_copy_files [3] and quartus_copy_files [4[] the relative source directory is the hdllib.cfg location and the relative destination directory is the build location. The direction can be from build dir to hdllib.cfg location or vice versa, or to any directory location in case absolute paths are used. The destination directory will be removed if it already exists, but only if it is within in the build location. If the destination directory is not in the build location, then it first needs to be removed manually to avoid accidentally removing a directory tree that should remain.

---
## 3 Examples of hdllib.cfg configuration files

### 3.1 Example for a simple HDL library
This shows a HDL library that contains firmware code for reading out sensors via I2C. The HDL library is called 'sens' at the ***hdl_lib_name*** key and from the ***hdl_lib_uses_synth*** key it follows that it dependents on the HDL libraries 'common' and 'i2c'. In VHDL the library 'sens' is declared by 'sens_lib', as defined by the ***hdl_library_clause_name*** key. The part of the 'sens' library that can be synthesized is listed at the ***synth_files*** key. The 'sens' library has a test bench for simulation listed at the ***test_bench_files*** key. The test bench is also listed at the ***regression_test_vhdl*** key, because it is suitable for a regression test, because it is self-checking and self-stopping.

```
hdl_lib_name = sens  
hdl_library_clause_name = sens_lib  
hdl_lib_uses_synth = common i2c  
hdl_lib_uses_sim =  
hdl_lib_technology =  

synth_files =  
    src/vhdl/sens_ctrl.vhd  
    src/vhdl/sens.vhd  
    
test_bench_files =  
    tb/vhdl/tb_sens.vhd  
    
regression_test_vhdl =  
    tb/vhdl/tb_sens.vhd  
    
[modelsim_project_file]  

[quartus_project_file]  

```

### 3.2 Example for a HDL library that copies files

This fft library contains reference data that is used in the test bench to verify the HDL in simulation. This reference data is copied from the source directory tree relative to where this hdllib.cfg is located, to the modelsim/fft sub directory in the build tree. The modelsim_config will create the data/ subdirectory in the build tree if it did not exist. If the data/ directory already exists, then it will just copy the files to it. Note that it is possible to list multiple copy from-to location pairs at the ***modelsim_copy_files*** key.

```
hdl_lib_name = fft  
hdl_library_clause_name = fft_lib  
hdl_lib_uses_synth = common mm dp diag rTwoSDF st  
hdl_lib_uses_sim =  
hdl_lib_technology =  

synth_files =  
    src/vhdl/fft_pkg.vhd  
    src/vhdl/fft_sepa.vhd  
    src/vhdl/fft_reorder_sepa_pipe.vhd  
    src/vhdl/fft_sepa_wide.vhd  
    src/vhdl/fft_r2_bf_par.vhd  
    src/vhdl/fft_r2_pipe.vhd  
    src/vhdl/fft_r2_par.vhd  
    src/vhdl/fft_r2_wide.vhd  
    src/vhdl/fft_wide_unit_control.vhd  
    src/vhdl/fft_wide_unit.vhd  
    
test_bench_files =  
    tb/vhdl/tb_fft_pkg.vhd  
    tb/vhdl/tb_fft_functions.vhd  
    tb/vhdl/tb_fft_sepa.vhd  
    tb/vhdl/tb_fft_reorder_sepa_pipe.vhd  
    tb/vhdl/tb_fft_r2_bf_par.vhd  
    tb/vhdl/tb_fft_r2_pipe.vhd  
    tb/vhdl/tb_fft_r2_par.vhd  
    tb/vhdl/tb_fft_r2_wide.vhd  
    tb/vhdl/tb_fft_wide_unit.vhd  
    tb/vhdl/tb_mmf_fft_r2.vhd  
    tb/vhdl/tb_mmf_fft_wide_unit.vhd  
    tb/vhdl/tb_tb_fft_r2_pipe.vhd  
    tb/vhdl/tb_tb_fft_r2_par.vhd  
    tb/vhdl/tb_tb_fft_r2_wide.vhd  

regression_test_vhdl =  
    tb/vhdl/tb_tb_fft_r2_pipe.vhd  
    tb/vhdl/tb_tb_fft_r2_par.vhd  
    tb/vhdl/tb_tb_fft_r2_wide.vhd  
 
[modelsim_project_file]  
modelsim_copy_files =  
    ../rTwoSDF/tb/data data  
    tb/data data  
    
[quartus_project_file]  
```

### 3.3 Example for a technology wrapper HDL library

This tech_fifo is a HDL library that VHDL files that wrap IP generated for a FIFO. The FIFO IP for different FPGA types is kept in dedicated HDL libraries. The tech_fifo provides a technology independent interface to these IP FIFOs. Dependent on a technology selection parameter only one IP FIFO will be used. The tech_fifo.vhd does declare all IP HDL libraries at the VHDL LIBRARY clause, to ensure default binding in simulation. The LIBRARY clause is ignored by synthesis. The ***hdl_lib_disclose_library_clause_names*** is used avoid a simulation error for the IP FIFO libraries that are not selected, when the tech_fifo is used in a particular application.

```
hdl_lib_name = tech_fifo  
hdl_library_clause_name = tech_fifo_lib  
hdl_lib_uses_synth = technology ip_stratixiv_fifo ip_arria10_fifo ip_arria10_e3sge3_fifo ip_arria10_e1sg_fifo  
hdl_lib_uses_sim =  
hdl_lib_technology =  
hdl_lib_disclose_library_clause_names =  
    ip_stratixiv_fifo      ip_stratixiv_fifo_lib  
    ip_arria10_fifo        ip_arria10_fifo_lib  
    ip_arria10_e3sge3_fifo ip_arria10_e3sge3_fifo_lib  
    ip_arria10_e1sg_fifo   ip_arria10_e1sg_fifo_lib  
 
synth_files =  
    tech_fifo_component_pkg.vhd  
    tech_fifo_sc.vhd  
    tech_fifo_dc.vhd  
    tech_fifo_dc_mixed_widths.vhd  
 
test_bench_files =  
 
regression_test_vhdl =  
    # no self checking tb available yet  
 
[modelsim_project_file]  
  
[quartus_project_file]  
```

### 3.4 Example for a board HDL library

This board HDL library serves as a board support package (BSP) that can be used for all applications that run on the FPGA. Hence a top level design with only this BSP servers as a minimal design for that FPGA board. The unb1_board library shows how the ***hdl_lib_include_ip*** key is used to for example only include the ip_stratixiv_tse_sgmii_lvds IP library for the Tripple Speed Ethernet IP and not the other IP that also exists in the technology wrapper for the 1GbE IP.

```
hdl_lib_name = unb1_board
hdl_library_clause_name = unb1_board_lib
hdl_lib_uses_synth = common dp diag uth ppsh i2c tr_nonbonded eth remu technology tech_pll epcs
hdl_lib_uses_sim = 
hdl_lib_technology = ip_stratixiv
hdl_lib_include_ip = ip_stratixiv_tse_sgmii_lvds
                     ip_stratixiv_pll
                     ip_stratixiv_pll_clk25

synth_files =
    src/vhdl/unb1_board_pkg.vhd  
    src/vhdl/unb1_board_system_info.vhd  
    src/vhdl/unb1_board_system_info_reg.vhd  
    src/vhdl/mms_unb1_board_system_info.vhd  
    src/vhdl/unb1_board_clk_rst.vhd  
    src/vhdl/unb1_board_clk200_pll.vhd  
    src/vhdl/unb1_board_clk25_pll.vhd  
    src/vhdl/unb1_board_wdi_extend.vhd  
    src/vhdl/unb1_board_node_ctrl.vhd  
    src/vhdl/unb1_board_sens_ctrl.vhd  
    src/vhdl/unb1_board_sens.vhd  
    src/vhdl/unb1_board_sens_reg.vhd  
    src/vhdl/mms_unb1_board_sens.vhd  
    src/vhdl/unb1_board_wdi_reg.vhd  
    src/vhdl/ctrl_unb1_board.vhd  
    
    src/vhdl/unb1_board_front_io.vhd  
    
    src/vhdl/unb1_board_mesh_io.vhd  
    src/vhdl/unb1_board_mesh_reorder_tx.vhd  
    src/vhdl/unb1_board_mesh_reorder_rx.vhd  
    src/vhdl/unb1_board_mesh_reorder_bidir.vhd  
    src/vhdl/unb1_board_mesh_uth_terminals_bidir.vhd  
    
    src/vhdl/unb1_board_back_io.vhd  
    src/vhdl/unb1_board_back_select.vhd  
    src/vhdl/unb1_board_back_reorder.vhd  
    src/vhdl/unb1_board_back_uth_terminals_bidir.vhd  
    
    src/vhdl/unb1_board_terminals_mesh.vhd  
    src/vhdl/unb1_board_terminals_back.vhd  
    
    src/vhdl/unb1_board_peripherals_pkg.vhd  

    # For BN the $RADIOHDL/boards/uniboard1/designs/unb1_bn_terminal_bg/src/vhdl/node_unb1_bn_terminal_bg.vhd is referred to directly in the apertif_unb1_bn_filterbank library. For FN a copy of $RADIOHDL/boards/uniboard1/designs/unb1_fn_terminal_db/src/vhdl/node_unb1_fn_terminal_db.vhd is taken via this unb1_board library:  
    
    src/vhdl/node_unb1_fn_terminal_db.vhd  
    
test_bench_files =  
    tb/vhdl/tb_unb1_board_pkg.vhd  
    
    tb/vhdl/tb_mms_unb1_board_sens.vhd  
    tb/vhdl/tb_unb1_board_clk200_pll.vhd  
    tb/vhdl/tb_unb1_board_node_ctrl.vhd  
    
    tb/vhdl/unb1_board_mesh_model_sosi.vhd  
    tb/vhdl/unb1_board_mesh_model_siso.vhd  
    tb/vhdl/unb1_board_mesh_model_sl.vhd  
    tb/vhdl/unb1_board_back_model_sosi.vhd  
    tb/vhdl/unb1_board_back_model_sl.vhd  
   
    tb/vhdl/tb_unb1_board_mesh_reorder_bidir.vhd  
    
    tb/vhdl/tb_tb_tb_unb1_board_regression.vhd  

regression_test_vhdl = 
    tb/vhdl/tb_mms_unb1_board_sens.vhd  
    tb/vhdl/tb_unb1_board_mesh_reorder_bidir.vhd  
 
[modelsim_project_file]  
 
[quartus_project_file]  
``` 

### 3.5 Example for a board HDL top level 'mother' design library

This top level 'mother' design library uses the 'unb1_board' board library to construct a design with only the Board Support Package functionality, that can serve as a minimal design that can run on the FPGA board. However this unb1_board library is not used directly, instead it is used as a 'mother' design libnrary that is used in revisions.

```
hdl_lib_name = unb1_minimal
hdl_library_clause_name = unb1_minimal_lib
hdl_lib_uses_synth = common mm unb1_board 
hdl_lib_uses_sim = 
hdl_lib_technology = ip_stratixiv

synth_files =
    $HDL_BUILD_DIR/unb1/quartus/unb1_minimal_sopc/sopc_unb1_minimal.vhd
    src/vhdl/qsys_unb1_minimal_pkg.vhd
    src/vhdl/mmm_unb1_minimal.vhd
    src/vhdl/unb1_minimal.vhd
    
test_bench_files = 
    tb/vhdl/tb_unb1_minimal.vhd

[modelsim_project_file]
modelsim_copy_files = 

[quartus_project_file]
quartus_copy_files = 
```

### 3.6 Example for a board HDL top level revision design library

This top level revision design library 'unb1_minimal_qsys' uses the 'mother' design library 'unb1_minimal' to construct a design that can be synthesized and run an FPGA of the UniBoard1 [5]. Therefore this 'unb1_minimal_qsys' HDL design library uses the [quartus_project_file] section to declare the keys that are needed to configure the synthesis.

```
hdl_lib_name = unb1_minimal_qsys  
hdl_library_clause_name = unb1_minimal_qsys_lib  
hdl_lib_uses_synth = unb1_board unb1_minimal  
hdl_lib_uses_sim =  
hdl_lib_technology = ip_stratixiv  
 
synth_files =  
    unb1_minimal_qsys.vhd  
    
test_bench_files =  
    tb_unb1_minimal_qsys.vhd  
    tb_unb1_minimal_qsys_stimuli.vhd  
 
regression_test_vhdl =  
    tb_unb1_minimal_qsys_stimuli.vhd  
 
[modelsim_project_file]  

[quartus_project_file]  
synth_top_level_entity =  
 
quartus_copy_files =  
    ../../quartus/qsys_unb1_minimal.qsys .  
 
quartus_qsf_files =  
    $RADIOHDL/boards/uniboard1/libraries/unb1_board/quartus/unb1_board.qsf  
 
quartus_sdc_files =  
    $RADIOHDL/boards/uniboard1/libraries/unb1_board/quartus/unb1_board.sdc  
    
quartus_tcl_files =  
    ../../quartus/unb1_minimal_pins.tcl  
    
quartus_vhdl_files =  
 
quartus_qip_files =  
    $HDL_BUILD_DIR/unb1/quartus/unb1_minimal_qsys/qsys_unb1_minimal/synthesis/qsys_unb1_minimal.qip  
```

Note: With another top level revision design library called 'unb1_minimal_sopc' it is possible to synthesize the same functionality, but than using the SOPC tool instead of QSYS tool from Altera to created the IP block for the memory mapped bus with NiosII master.

### 3.7 Example for a HDL library for pre-generating and simulating IP

The ***qmegawiz_ip_files*** key in this 'ip_stratixiv_mac_10g' IP library is used by **generate_ip_libs** to know that it can pre-generate a 10GbE MAC for the strativiv FPGA. The ***modelsim_compile_ip_files*** will be used by **modelsim_config*** to include the compile_ip.tcl script when this library is compiled in Modelsim. The compile_ip.tcl refers to simualtion models for the 10GbE MAC.

```
hdl_lib_name = ip_stratixiv_mac_10g  
hdl_library_clause_name = ip_stratixiv_mac_10g_lib  
hdl_lib_uses_synth = common  
hdl_lib_uses_sim =  
hdl_lib_technology = ip_stratixiv  
  
synth_files =  
    
test_bench_files =  
 
[modelsim_project_file]  
modelsim_copy_files =  
    
modelsim_compile_ip_files =  
    $HDL_WORK/libraries/technology/ip_stratixiv/mac_10g/compile_ip.tcl    
  
[quartus_project_file]  
quartus_copy_files =  
    
quartus_vhdl_files =  
    ip_stratixiv_mac_10g.vhd  
 
quartus_sdc_files =  
  
quartus_qip_files =  
    ip_stratixiv_mac_10g.qip  
 
[generate_ip_libs]  
qmegawiz_ip_files =  
    ip_stratixiv_mac_10g.vhd  
```
 
