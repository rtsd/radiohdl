# RadioHDL buildset for alveo revisions

---
#### Document history:
|Revision|Date|Author|Affiliation|Modification|
|:---|:---|:---|:---|:---|
|0.1|27 feb 2023|R. van der Walle|ASTRON|Create.|

---
#### Contents:
1 Introduction  
2 Workflow  
3 Appendix: hdl_buildset_alveo.cfg  

#### References:
[1] radiohdl_user_guide.md  

---
# 1 Introduction

This RadioHDL buildset for Alveo user guide describes how to use RadioHDL for developing HDL applications on Alveo. The RadioHDL User Guide describes the general concepts of RadioHDL [1].

---
# 2 Workflow for Alveo

For now, it is only possible to generate the include commands for an existing Vivado project. For Alveo do:
```
  > vivado_config alveo
  > source the generated <lib_name>_include.tcl from $HDL_BUILD_DIR/alveo/vivado/<lib_name>/
```

To simulate for Alveo do:
```
  > run_modelsim alveo &
```

Synthesize for Alveo is not yet supported by RadioHDL:
```
  
```

---
# 3 Appendix: hdl_buildset_alveo.cfg

Buildset configuration files are kept at $RADIOHDL_GEAR/config.

```
buildset_name             = alveo
technology_names          = ip_ultrascale
family_names              = ultrascale
block_design_names        = bd

synth_tool_name           = vivado
synth_tool_version        = 2022.1
sim_tool_name             = modelsim
sim_tool_version          = 10.4

#lib_root_dirs             = ${HDL_WORK}/libraries ${HDL_WORK}/applications ${HDL_WORK}/boards
lib_root_dirs             = ${HDL_WORK}/libraries
                            ${HDL_WORK}/applications/lofar1/RSP/pfb2
                            ${HDL_WORK}/applications/lofar1/RSP/pfs
                            ${HDL_WORK}/applications/lofar1/RSP/pft2
                            
#[vivado]
#vivado_dir                = ${XILINX_DIR}/<synth_tool_version>

[modelsim]
modelsim_dir              = ${MENTOR_DIR}/<sim_tool_version>/questasim
modelsim_platform         = linux_x86_64
modelsim_search_libraries =
```

