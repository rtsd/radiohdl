### *RadioHDL GIT Regressiontest*

#### Document history:
|Revision|Date|Author|Affiliation|Modification|
|:---|:---|:---|:---|:---|
|0.1 |18 aug 2020 |P. Donker |ASTRON |Initial radiohdl regressiontest.md |

#### Contents:
1. Introduction
2. Installing
3. Running the modelsim regression test.
3.1 Running the test manualy.
3.2 Running the test by a cron job.
4. How *modelsim_regression_test_vhdl.py* works.
5. How *modelsim_regression_test_vhdl_mail.py* works.
6. How *modelsim_regression_test_vhdl_cron.sh* works.
7. Example of setting it up on a new pc.


#### Terminology:
- *buildset*: boardname

---

## 1 Introduction
Modelsim regression test for GIT repository is part of Radiohdl tooling and uses information in the *hdllib.cfg* files to do the tests,
It will only run the test benches listed under the  *regression_test_vhdl*  key.

It can be used on the commandline using  *modelsim_regression_test_vhdl.py*  to test a single vhdl library or all libraries in a *buildset*,
output on the screen and log files are made for each test bench in the build directory.
Or run by a cron job calling  *modelsim_regression_test_vhdl_cron.sh*  on a regular base. It will update git, run the test and mail the result.

The Regressiontest excists of 3 main scripts, 2 template files and a mailing list.
The scripts and templates are located in `/radiohdl/regressiontest/` directory.
- modelsim_regression_test_vhdl_cron.sh
- modelsim_regression_test_vhdl.py
- modelsim_regression_test_vhdl_mail.py
- do_mk_all.tmpl
- do_simulation.tmpl
- modelsim_regression_test_mail_list.txt

---

## 2 Installing.
To install the git regression test on a new pc takes the following steps:
1. make a new account on the test pc, we use regtest as name and password.
> mkuser regtest
2. add following lines to your *.bashrc* file
> \# Altera + ModelSim licenses
> export LM_LICENSE_FILE=[your license server settings]

> \# Altera, Mentor and modelsim_altera_libs dir
> export ALTERA_DIR=[path to Altera dir]
> export MENTOR_DIR=[path to Mentor dir]
2. add regtest to the sudoers list for crontab.
> sudo vi /etc/sudoers
    add to bottom
> regtest ALL=(ALL) NOPASSWD:/usr/bin/crontab 
3. get an account on the git repository and setup git user on this pc.
    you may need a ssh key, generate one without a password.
4. checkout radiohdl from git.
> cd ~/git
> git clone [radiohdl url]
5. checkout hdl from git.
> git clone [hdl url]
6. add a cron job
> sudo crontab -e -u regtest`
    add line for test every morning 2:00
> 0 2 * * * /home/regtest/git/radiohdl/regressiontest/modelsim_regression_test_vhdl_cron.sh unb2b > regtest.log 2>&1

---

## 3 Running the modelsim regression test
The regression test can be run in 2 different ways.
1. by hand on the command line using  *modelsim_regression_test_vhdl.py*
2. by running a cron job on a regular base and calling  *modelsim_regression_test_vhdl_cron.py*

### 3.1 Running the test manualy.
The tests are done on the active GIT repository.

For all libraries in <buildset> use.
> $HOME/git/radiohdl/regressiontest/modelsim_regression_test_vhdl.py <buildset>

Test only --libnames <libnames> in <buildset> use.
> $HOME/git/radiohdl/regressiontest/modelsim_regression_test_vhdl.py <buildset> --libnames <library name(s)>

### 3.2 Running the test by a cron job.
Tests are done on a fresh GIT master branch.

Test all libraries in <buildset> for *$HOME/git/radiohdl/regressiontest/modelsim_regression_test_vhdl_cron.sh <buildset>*
Adding a line to crontab to run every monday at 4:00 start crontab and add *0 4 \* \* 1 /home/<username>/git/radiohdl/regressiontest/modelsim_regression_test_cron.sh <buildset> 2>&1*

for username *regtest* and buildset *unb2b*.
> sudo crontab -e -u regtest
> 0 4 * * 1 /home/regtest/git/radiohdl/regressiontest/modelsim_regression_test_cron.sh unb2b 2>&1
---

### 4 How *modelsim_regression_test_vhdl.py* works.
This test script is using worker procresses to do the work, default one worker is used, but can be increased by the option -p, --proc 1..8.
For each worker a licence is needed. One task excists of a library that can contain more than one testbench.

Only test benches listed under  *regression_test_vhdl*  key in  *hdllib.cfg*  are done.

The regressiontest is done in 3 steps:

1. cleanup old do and log files and create new do files, *<libname>_mk_all.do* and *<libname>_simulation.do*
2. run all *<libname>_mk_all.do* files using only one process.
3. run all *<libname>_simulation.do* files using all available processes.

step 1 and 2 are done using only 1 worker because `mk all` can not be run twice for the same lib at the same time.
step 3 is using all available workers to do the job.

This script ends with generating *modelsim_regression_test_vhdl_email.txt* in *git/hdl/build/<buildset>/modelsim/*.

do files and logging of each do file can be found in the build dir.
> $RADIHDL_BUILD/<buildset>/modelsim/<libname>/regression_test_vhdl/*

---

### 5 How *modelsim_regression_test_vhdl_mail.py* works.
*modelsim_regression_test_vhdl_mail.py* is the script that sends out the short test results made by *modelsim_regression_test_vhdl.py*
 to the email recipients listed in *modelsim_regression_test_mail_list.txt`*.

---

### 6 How *modelsim_regression_test_vhdl_cron.sh* works.
*modelsim_regression_test_vhdl_cron.sh* is the main script that is called by a cron job.
The following tasks are done for <buildset>:
- update git from remote repository
- cleanup <buildset> build directory
- call *compile_altera_simlibs*
- call *generate_ip_libs*
- call *modelsim_config*
- call *quartus_config*
- call *modelsim_regression_test_vhdl.py*
- call *modelsim_regression_test_vhdl_mail.sh*

Output of all scripts can be found in `$HOME/<buildset>_modelsim_regression_test_vhdl_cron.log`

---

### 7 Example, steps I did to install a working version of modelsim regressiontest as a cron job on a new PC.
This is done for the ASTRON vhdl git repository.

1. Installing quartus and modelsim in /home/software/
2. Making a new account for regtest with password regtest.
> mkuser regtest
3. Adding the following lines to the *.bashrc* file
> \# Altera + ModelSim licenses
> export LM_LICENSE_FILE=1800@license4.astron.nl:1717@license5.astron.nl
>     
> \# Altera, Mentor and modelsim_altera_libs dir
> export ALTERA_DIR=/home/software/Altera
> export MENTOR_DIR=/home/software/Mentor
4. Adding regtest to the bottom of the sudoers list */etc/sudoers* for crontab.
> regtest ALL=(ALL) NOPASSWD:/usr/bin/crontab
5. Generating a ssh key without a passphrase.
> ssh-keygen
6. Getting an account on the git repository and setup git user on this pc.
> copy ssh public key to gitlab
> git config --global user.name "regtest"
> git config --global user.email regtest@astron.nl
7. Checkout radiohdl from git.
> cd ~/git
> git clone git@git.astron.nl:rtsd/radiohdl.git
8. Checkout hdl from git.
> git clone git@git.astron.nl:rtsd/hdl.git
9. Add a cron job using  *sudo crontab -e -u regtest*  add line for test every morning 2:00
> 0 2 * * * /home/regtest/git/radiohdl/regressiontest/modelsim_regression_test_vhdl_cron.sh unb2b > regtest.log 2>&1
