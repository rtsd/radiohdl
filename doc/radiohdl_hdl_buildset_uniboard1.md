# RadioHDL buildset for UniBoard1

---
#### Document history:
|Revision|Date|Author|Affiliation|Modification|
|:---|:---|:---|:---|:---|
|1.0|27 jan 2020|E. Kooistra|ASTRON|Created.|

---
#### Contents:
1 Introduction  
1.1 Scope  
1.2 RadioHDL commands  
2 Workflow  
2.1 Preconditions  
2.2 Setup RadioHDL  
2.2.1 Source project settings  
2.2.2 Source RadioHDL settings  
2.3 Check the RadioHDL configuration  
2.4 Create intermediate product files and project files  
2.4.1 Compile standard Altera component libraries for Modelsim simulation  
2.4.2 Pre-generate custom Altera IP blocks for simulation and synthesis  
2.4.3 Create project files for simulation  
2.4.4 Create project files for synthesis  
2.4.5 Create an IP block design for a design library in your project  
2.5 Simulate the HDL project  
2.6 Synthesize the HDL project  
3 Appendix: hdl_buildset_unb1.cfg  

#### References:
[1] radiohdl_user_guide.md
[2] A. Gunst, A. Szomoru, G. Schoonderbeek, E. Kooistra, D. van der Schuur, H. Pepping, "The application of UniBoard as a beam former for APERTIF", Experimental Astronomy, Volume 37, Issue 1, pp 55-67, February 2014, doi:10.1007/s10686-013-9366-x.
[3] radiohdl_hdl_tool_modelsim.md  
[4] radiohdl_hdl_tool_quartus.md  
[5] radiohdl_hdl_buildset_key_descriptions.md  

---
# 1 Introduction

## 1.1 Scope
The RadioHDL package offers scripts to ease and speed up HDL code development. The RadioHDL User Guide describes the general concept s of RadioHDL [1]. This RadioHDL for UniBoard1 user guide describes how to use RadioHDL for developing HDL applications on UniBoard1. The UniBoard1 [2] is an electronic board with 8 Altera Stratix IV FPGAs, that was developed by ASTRON and JIVE within the RadioNET FP7 program,

This user guide serves as an advanced start example for other projects. It describes how RadioHDL is used for the UniBoard1 at ASTRON (2019) to develop HDL firmware from source code to working FPGA image. There is too much HDL code involved and too much dependency on vendor tools to make this example publicly available as a working example. For a publicly working example please try the quick start example from [1].

## 1.2 RadioHDL commands
The HDL development for UniBoard1 involves vendor tools from Mentor/Modelsim for simulation and Intel/Altera for synthesis. For this RadioHDL offers the following command line commands:

* **check_config** to verify dependencies between hdl_buildset and hdl_tool configuration files
* **compile_altera_simlibs** to compile standard Altera component libraries for simulation with Modelsim
* **generate_ip_libs** to pre-generate customized IP components using Altera MegaWizard or Altera QSYS
* **modelsim_config** to create project files for Modelsim simulation
* **quartus_config** to create project files for Quartus synthesis
* **run_sopc** to generate a Altera SOPC memory mapped bus with NiosII
* **run_qsys** to generate a Altera QSYS memory mapped bus with NiosII
* **run_modelsim** to start Mentor Modelsim GUI for simulation
* **run_quartus** to start Altera Quartus GUI for synthesis
* **run_qcomp** to synthesize a top level design from the command line
* **run_rbf** to create an Altera FPGA rbf file from an FPGA sof file

The scripts that start with 'run_' typically are bash scripts that invoke tools. The other scripts are typically Python3 scripts that manage the build directory tree and e.g. tool project files. Please see [3] and [4] for more information on how to use Modelsim and Quartus with RadioHDL. Please see [5] for hdllib.cfg configuration file examples that are used by **generate_ip_libs**, **modelsim_config** and **quartus_config**.

---
## 2 Workflow

### 2.1 Preconditions

RadioHDL uses Python3 and bash. Python3 is used to interpret the configuration files and to create target files, bash is used to setup and invoke tools. Install a version of:

- Modelsim in ```$MENTOR_DIR/<version id>``` (Modelsim version 6.6c or higher) 
- Quartus in $ALTERA_DIR (version 11.1 for UniBoard1) 

and take care of all vendor specific environment variables in your own .bashrc or local .profile file. For example Altera and Modelsim needs the key LM_LICENSE_FILE to be defined. So the .bashrc file contains the lines:
```
  export MENTOR_DIR=<path to where Mentor tool versions are installed>
  export ALTERA_DIR=<path to where Altera tool versions are installed>
  export LM_LICENSE_FILE=<license@our_company>
```

### 2.2 Setup RadioHDL

#### 2.2.1 Source Project settings
The UniBoard1 HDL source code is available in a source directory tree at hdl/. Setup RadioHDL development environment for a project at hdl/ using a corresponding init_hdl.sh.
```
  > cd <path to>/hdl
  > . ./init_hdl.sh
```
The init_hdl.sh defines:

- HDL_WORK directory for where the source code resides
- HDL_BUILD_DIR directory for where the targets will be build
  
The init_hdl.sh copies version controlled ${HDL_WORK}/hdl_user_components.ipx into the Altera dir's at ```$ALTERA_DIR/<tool version>/ip/altera/user_components.ipx```. This Altera user_components.ipx contains the search paths that Altera SOPC builder and Altera QSYS use.

> **NOTE:** Altera says that you can use your personal user_components.ipx file in ```~/.altera.quartus/ip/<quartus_version>/ip_search_path``` for this kind of additional paths but tests show that this does not work for all tools of Altera. Therefore the user_components.ipx file is copied to the Altera installation directory.

#### 2.2.2 Source RadioHDL settings

The init_hdl.sh can automatically also source init_radiohdl.sh, or source these RadioHDL settings separately:
```
  > . <path to RadioHDL installation>/init_radiohdl.sh
```
The init_radiohdl.sh from the RadioHDL package defines:

- RADIOHDL_GEAR directory of where the init_radiohdl.sh is located
- HDL_BUILD_DIR = ${HDL_BUILD_DIR}/build if not already defined
- RADIOHDL_CONFIG = ${RADIOHDL_GEAR}/config if not already defined
                                
The init_radiohdl.sh extends:

- PATH with ${RADIOHDL_GEAR}/core, ${RADIOHDL_GEAR}/quartus, ${RADIOHDL_GEAR}/modelsim
- PYTHONPATH with ${RADIOHDL_GEAR}/core, ${RADIOHDL_GEAR}/components

### 2.3 Check the RadioHDL configuration

The hdl_buildset_unb1.cfg defines the buildset 'unb1' for build target files for UniBoard1. Since there are some dependencies between the hdl_buildset configuration file and the hdl_tool configuration files there is a utility that checks (most) values in these files. To check the content of the file hdl_buildset_unb1.cfg and the hdl_tool_modelsim.cfg and hdl_tool_quartus.cfg files it refers to do
```
  > check_config unb1  
```
Once the configuration files are correct you can start using the RadioHDL utilities.


### 2.4 Create intermediate product files and project files

#### 2.4.1 Compile standard Altera component libraries for Modelsim simulation

This only needs to be done once when a new version of Modelsim or new version of Quartus is used.
```
  > compile_altera_simlibs unb1
```
Creates:  

* file build/unb1/hdl_libraries_ip_stratixiv.txt that maps each library to its location
* the ```build/quartus/<tool version>``` simulation models of Altera components that need to be moved manually using 'mv' or 'sudo mv' to ```$MODELSIM_ALTERA_LIBS_DIR/<tool version>```, because that is where Modelsim expects them.

> **NOTE:** For Modelsim versions newer than about version10 this compile_altera_simlibs script must be used and not the tools/Launch simulation library compiler in the Quartus GUI, because the libraries have to be compiled with the 'vlib -type directory' option to be able to use 'mk all' in Modelsim.

#### 2.4.2 Pre-generate custom Altera IP blocks for simulation and synthesis

The custom IP needs to be generated before it can be simulated or synthesized. To make all custom IP libraries for the 'unb1' buildset do:
```
  > generate_ip_libs unb1
```
Creates:  

* directory build/unb1/qmegawiz/  

> **NOTE:** The Altera tools make use of the /tmp directory intensively. Unfortunately these tools do not cleanup the files they created there. We ran into major problems when working with more than one user on the same machine:  
> 1. /tmp/compute_pll_temp and /tmp must have write-access for anyone (rwxrwxrwx)  
> 2. run ```rm -rf /tmp/alt* /tmp/public*``` before running generate_ip_libs  


#### 2.4.3 Create project files for simulation
```
  > modelsim_config unb1
```
Creates:  

* directories ```build/unb1/modelsim/<hdllib libraries>``` with Modelsim project files (mpf) for simulation
* file build/unb1/modelsim/modelsim_project_files.txt for Modelsim commands.do, that lists all HDL libraries in their hierarchical order for compilation.
* file build/unb1/modelsim/technology_select_pkg.vhd to select stratixiv technology for UniBoard1


#### 2.4.4 Create project files for synthesis
```
  > quartus_config unb1
```
Creates:

* directories ```build/unb1/quartus/<hdllib libraries>``` with Quartus files for synthesis
* file build/unb1/quartus/technology_select_pkg.vhd to select stratixiv technology for UniBoard1

The quartus_config creates the Quartus qpf, qsf for a top level design library and Quartus qip files all libraries. If a new VHDL file is added to a library or if a filename is changed, then it is necessary to rerun the quartus_config. Typically it is not necessary to do delete the entire $HDL_BUILD_DIR library of the design directory, but it can be necessary to make sure that Quartus is not 'confused' by obsolete files in that build directory. Make sure that any image files that needs to be kept are saved elsewhere, before deleting the build directory of the design library.


#### 2.4.5 Create an IP block design for a design library in your project

Dependent of which tool you like to use you can run run_sopc (older) or run_qsys (newer) to generate the control interface of your design.
```
  > run_sopc unb1 unb1_minimal_sopc
```
Creates the memory mapped (MM) bus block with NiosII master using Altera SOPC
```
  > run_qsys unb1 unb1_minimal_qsys  
```
Creates the memory mapped (MM) bus block with NiosII master using Altera QSYS

Syntax:
```
  > run_qsys <buildset> <project> [<qsysfile>]  
  > run_sopc <buildset> <project> [<sopcfile>]  
```
Both utilities only differ in name not in functionality. The ```<project>``` is a HDL library directory that needs to exist under $HDL_BUILD_DIR/${buildset}/quartus. If no ```<qsysfile>``` respectively ```<sopcfile>``` is specified during the invocation, then the utility will look for a file with the name ```<project>.qsys``` or ```<project>.sopc``` in the project directory.

After generating all necessary files the utility will also build the software for the NiosII application using the RadioHDL run_app script. This run_app calls run_bsp and run_reg. The run_reg calls run_mif. For the FPGA designs on UniBoard the NiosII software application is fixed, because it implements the monitoring and control protocol interface between the 1GbE port and the memory mapped bus that is generated by SOPC or QSYS. The NiosII is the master on the memory mapped bus, and the slaves are configurable dependent on the design.

> **NOTE:** The sopc_builder from Altera uses Java and Java expects that it is started from an Xterm (environment DISPLAY must be set) otherwise it stops with an error.


### 2.5 Simulate the HDL project

To simulate test benches within the hdl project for UniBoard using the Modelsim GUI do:
```
  > run_modelsim unb1 &  
```
Please see [3] for a set of useful commands that RadioHDL adds to the Modelsim Transcript window. In simulation the entire unb1_minimal design can be simulated. The NiosII and memory mapped bus are not simulated. Instead the access to the memory mapped slaves is simulated using file IO.

### 2.6 Synthesize the HDL project

The way to start quartus is using the script **run_quartus**. This script sets up the environment variables that match the buildset and then start quartus itself [4]. To synthesize a top level design within the hdl project for UniBoard using the Quartus GUI do:
```
  > run_quartus unb1 &  
```
To synthesize the unb1_minimal_sopc top level design from the command line do:
```
  > run_qcomp unb1 unb1_minimal_sopc  
```
To synthesize the unb1_minimal_qsys top level design from the command line do:
```
  > run_qcomp unb1 unb1_minimal_qsys  
```
The run_qcomp script can also rerun the synthesis for multiple seeds by using its --seed option. To translate the FPGA image sof file into an rbf file for use in a flash on UniBoard do:
```
  > run_rbf unb1 unb1_minimal_sopc  
  > run_rbf unb1 unb1_minimal_qsys  
```

---
## 3 Appendix: hdl_buildset_unb1.cfg

Buildset configuration file for Uniboard1 from $RADIOHDL_GEAR/config.
```
buildset_name             = unb1  
technology_names          = ip_stratixiv  
family_names              = stratixiv  
block_design_names        = sopc  
  
synth_tool_name           = quartus  
synth_tool_version        = 11.1  
sim_tool_name             = modelsim  
sim_tool_version          = 6.6c  
  
lib_root_dirs             = ${HDL_WORK}/libraries  
                            ${HDL_WORK}/applications  
                            ${HDL_WORK}/boards  
  
[quartus]  
quartus_dir               = ${ALTERA_DIR}/<synth_tool_version>  
  
[modelsim]  
modelsim_dir              = ${MENTOR_DIR}/<sim_tool_version>/modeltech  
modelsim_platform         = linux_x86_64  
modelsim_search_libraries =  
    # stratixiv only  
    altera_ver lpm_ver sgate_ver altera_mf_ver altera_lnsim_ver  
    stratixiv_ver stratixiv_hssi_ver stratixiv_pcie_hip_ver  
    altera lpm sgate altera_mf altera_lnsim stratixiv stratixiv_hssi  
    stratixiv_pcie_hip  
```

