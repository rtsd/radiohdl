# RadioHDL buildset for UniBoard2 revisions

---
#### Document history:
|Revision|Date|Author|Affiliation|Modification|
|:---|:---|:---|:---|:---|
|0.1|13 feb 2020|E. Kooistra|ASTRON|Draft refers to workflow for unb1, and only describes where teh workflow for unb2 differs.|

---
#### Contents:
1 Introduction  
2 Workflow  
3 Appendix: hdl_buildset_unb1.cfg  

#### References:
[1] radiohdl_user_guide.md  
[2] G. W. Schoonderbeek, A. Szomoru, A. W. Gunst, L. Hiemstra, J. Hargreaves “UniBoard2, A Generic Scalable High-Performance Computing Platform for Radio Astronomy,” Journal of Astronomical Instrumentation, 8, 18, December 2018, pp. 0-0, doi:10.1142/S225117171950003X  
[3] radiohdl_hdl_buildset_uniboard1.md  

---
# 1 Introduction

This RadioHDL buildset for UniBoard2 user guide describes how to use RadioHDL for developing HDL applications on UniBoard2. The RadioHDL User Guide describes the general concepts of RadioHDL [1]. The UniBoard2 [2] is an electronic board with 4 Intel/Altera Arria10 FPGAs, that was developed by ASTRON and JIVE within the RadioNET FP7 program. There are different hardware revisions of the UniBoard2 that are identified by different buildset names: 'unb2a', 'unb2b', 'unb2c'. The workflow for the UniBoard2 revisions is quite similar and also similar to the workflow for the UniBoard1.

---
# 2 Workflow for UniBoard2

Please follow the workflow for UniBoard1 that is described in [3] as starting point, and use this user guide for UniBoard2 to know where the workflow for UniBoard2 differs. For unb2b do (similar for unb2c):
```
  > check_config unb2b
  > compile_altera_simlibs unb2b
  > generate_ip_libs unb2b
  > modelsim_config unb2b
  > quartus_config unb2b
```

The HDL development for UniBoard2 involves vendor tools from Mentor/Modelsim for simulation and Intel/Altera for synthesis. For UniBoard2 RadioHDL offers the following additional command line commands:

* **run_qsys_pro** to generate a Altera QSYS memory mapped bus with NiosII

To simulate for UniBoard2 do (similar for unb2c):
```
  > run_modelsim unb2b &
```

To synthesize UniBoard2 using GUi do (similar for unb2c, for synthesis from command line see [3]):
```
  > run_quartus unb2b &
```

---
# 3 Appendix: hdl_buildset_unb2b.cfg

Buildset configuration files are kept at $RADIOHDL_GEAR/config.

```
# Uniboard 2b configuration
buildset_name             = unb2b
technology_names          = ip_arria10_e1sg
family_names              = arria10
block_design_names        = qsys

synth_tool_name           = quartus
synth_tool_version        = 18.0
sim_tool_name             = modelsim
sim_tool_version          = 10.4

lib_root_dirs             = ${HDL_WORK}/libraries ${HDL_WORK}/applications ${HDL_WORK}/boards

[quartus]
quartus_dir               = ${ALTERA_DIR}/<synth_tool_version>

[modelsim]
modelsim_dir              = ${MENTOR_DIR}/<sim_tool_version>/questasim
modelsim_platform         = linux_x86_64
modelsim_search_libraries =
    # arria10 only
    altera_ver lpm_ver sgate_ver altera_mf_ver altera_lnsim_ver twentynm_ver twentynm_hssi_ver twentynm_hip_ver
    altera     lpm     sgate     altera_mf     altera_lnsim     twentynm     twentynm_hssi     twentynm_hip
    # both stratixiv and arria10
    #altera_ver lpm_ver sgate_ver altera_mf_ver altera_lnsim_ver stratixiv_ver stratixiv_hssi_ver stratixiv_pcie_hip_ver twentynm_ver twentynm_hssi_ver twentynm_hip_ver
    #altera     lpm     sgate     altera_mf     altera_lnsim     stratixiv     stratixiv_hssi     stratixiv_pcie_hip     twentynm     twentynm_hssi     twentynm_hip
```

