# RadioHDL tool settings for Modelsim

---
#### Document history:
|Revision|Date|Author|Affiliation|Modification|
|:---|:---|:---|:---|:---|
|1.0|27 jan 2020|E. Kooistra|ASTRON |Created.|

---
#### Contents:
1	Introduction  
1.1	Preconditions  
2	Creating Modelsim project files  
3	Using Modelsim  
3.1	Starting the Modelsim GUI  
3.2	Compiling the HDL  
3.3	Simulating the HDL  
3.4	Developing the HDL  
4	Appendix: Configuration files  
4.1	hdl_tool_modelsim.cfg  
4.2	hdllib.cfg keys for Modelsim  
5 Appendix: hdl_tool_modelsim.cfg

---
#### Terminology:

- FPGA = Field Programmable Gate Array
- GUI = Graphical User Interface
- HDL = Hardware Description Language  
- IP = Intellectual Property  
- MPF = Modelsim Project File  

---
#### References:
[1] radiohdl_user_guide.md  
[2] radiohdl_hdl_library_key_descriptions.md  
[3] radiohdl_hdl_tool_quartus.md  
[4] radiohdl_hdl_buildset_uniboard1.md

---  
## 1 Introduction
The RadioHDL package consists of a set of scripts that interpret configuration files and that setup or run other tools for HDL code development [1]. This document describes how to use the following RadioHDL tools for compiling and simulating HDL code with Mentor/Modelsim:

* **modelsim_config** : to create Modelsim project files (mpf) for simulation of HDL code
* **run_modelsim** : to start the Modelsim GUI and make and compile HDL code in Modelsim and hierarchically add signals to the Modelsim wave window
  
### 1.1 Preconditions
Setup the RadioHDL environment as explained in [1].

The RadioHDL for Modelsim is configured using a ```hdl_tool_<tool_name>.cfg``` = hdl_tool_modelsim.cfg configuration file (see appendix), where the tool_name = modelsim is defined in a ```hdl_buildset_<buildset_name>.cfg``` configuration file. Here assume we use the quick start example from [1], so with hdl_buildset_qs.cfg and hdl_tool_modelsim.cfg. The HDL source is grouped into one or more HDL libraries that are described by hdllib.cfg configuration files.

If the HDL uses vendor technology libraries then run the corresponding vendor specific script to compile these libraries and to create a ```hdl_libraries_ip_<technology_name>.cfg``` file, that modelsim_config uses to map the library paths. For quartus this is compile_altera_simlibs script [3] that e.g. creates ```hdl_libraries_ip_stratixiv.txt``` for buildset_name unb1 [4].

---
## 2 Creating Modelsim project files
The binaries for Modelsim are built in a separate directory tree under $HDL_BUILD_DIR.
Use modelsim_config to create the Modelsim project files (mpf) for all your HDL libraries. In this example the quick start buildset 'qs' is used:
```
  > modelsim_config -h
  > modelsim_config qs
```
The modelsim_config script puts the created files and copied files in the build directory, at:
```
$HDL_BUILD_DIR/<buildset_name>/<sim_tool_name>/<hdl_lib_name>
```
For 'buildset_name = qs' and 'sim_tool_name = modelsim' defined in hdl_buildset_qs.cfg this is at:
```
  $HDL_BUILD_DIR/qs/modelsim
```
The ```<hdl_lib_name>``` sub directories follow from the 'hdl_lib_name' key that is defined in the hdllib.cfg configuration files per HDL library. The modelsim_config script finds all hdllib.cfg files within 'lib_root_dirs = ${HDL_WORK}' that is defined in hdl_buildset_qs.cfg. The lib_root_dirs  can list one or more search trees.

Optionally you can clear this directory before running modelsim_config because everything is recreated:
```
  > rm -rf $HDL_BUILD_DIR/qs/modelsim
```
If the HDL library dependencies change, e.g. because a new VHDL file is added to a library, or if a filename is changed, then it is necessary to rerun the modelsim_config.

In addition to the Modelsim project files the modelsim_config also generates some auxiliary files that are used by commands.do in run_modelsim as quick reference to all HDL libraries. The central modelsim_project_files.txt at $HDL_BUILD_DIR/qs/modelsim/ contains a list of the paths to all HDL libraries. Per HDL library there is a ```<lib_name>_lib_order.txt``` file that contains a list of all HDL libraries  that this HDL library depends on in order.

---
## 3 Using Modelsim

### 3.1 Starting the Modelsim GUI

To support your work inside Modelsim, RadioHDL comes with a **command.do** file that adds the following useful commands to the Modelsim Transcript command line window:

* lp = load project
* mk = make project
* as = add signals to the wave window
* ds = delete signals from wave window

To use these commands with your buildset start the Modelsim GUI with your buildset_name using run_modelsim:
```
  > run_modelsim qs &
```

### 3.2 Compiling the HDL
First compile the dp_repack_data library and the libraries that it depends on.In the Modelsim Transcript window load Modelsim project file for HDL library dp_repack_data:
```
  ModelSim> lp dp_repack_data
```
Report current library:
```
  ModelSim> lp
```
Report all libraries in order that dp_repack_data depends on:
```
  ModelSim> lp all
```
Compile all libraries in order that dp_repack_data depends on:
```
  ModelSim> mk compile all
```
Compile only this dp_repack_data library:
```
  ModelSim> mk compile
```
To load another HDL library project e.g. common_pkg, do:
```
  ModelSim> lp common_pkg
```
Report all libraries in order that common_pkg depends on:
```
  ModelSim> lp all
```
Instead of 'mk compile' one can use GNU 'make' for Unix (or make.exe on Windows) and Modelsim 'vmake' via the 'mk' command from **commands.do**. The advantage is that after an initial successful compile of all, any subsequent recompiles after editing a HDL source file only will require recompilation of the HDL source files that depend on it. 

Load Modelsim project file for HDL library dp_repack_data:
```
  ModelSim> lp dp_repack_data
```
Clean build directories and makefiles in $HDL_BUILD_DIR that were made for dp_repack_data:
```
  ModelSim> mk clean all
```
Make all libraries in order that dp_repack_data depends on and create the makefiles for those libraries:
```
  ModelSim> mk all
```
To make only this dp_repack_data library do:
```
  ModelSim> mk
```
To make only the specific common_pkg library do:
```
  ModelSim> mk common_pkg
```
The first time 'mk all' is called the libraries are compiled and the library makefiles are made. The library makefiles are stored in the library build directories in $HDL_BUILD_DIR. It is important that all libraries compile OK, because otherwise the library makefiles are not created properly. Therefore if a library fails to compile, then load that library project with 'lp' and keep on doing 'mk compile' until the library compiles OK. Then when it compiles OK do 'mk clean' to clear the library build directory at $HDL_BUILD_DIR, and then do 'mk' or 'mk all' to compile the library again and create a proper makefile. Repeat this process until also the top level HDL library compiles OK.

With the proper makefiles for all HDL libraries it is sufficient to use 'mk' to automatically recompile only the HDL source files that changed or that depend on the changed file. Similar with 'mk all' a change in some lower level library HDL file will only cause that the HDL files that depend on it will be recompiled. For a big project with many libraries using 'mk all' is much easier and faster to use than 'mk compile all'. Doing 'mk all' and again 'mk all' should show that the second 'mk all' did not need to recompile any HDL again.

Note that the dp_repack_data library also depends on the common_pkg library. Therefore when doing the following:
```
  ModelSim> lp common_pkg
  ModelSim> mk clean all
  ModelSim> mk all
  ModelSim> lp dp_repack_data
  ModelSim> mk all
```
Then the 'mk all' in the dp_repack_data library will not recompile the libraries that were already compiled by the 'mk all' in the common_pkg library. Similar if you clean the common_pkg library while the dp_repack_data library project is loaded:
```
  ModelSim> lp dp_repack_data
  ModelSim> mk clean common_pkg
  ModelSim> mk all
```
Then the 'mk all' in the dp_repack_data library will recompile and recreate the makefile for the common_pkg library, and then reompile the HDL files in the higher libraries (up to dp_repack_data) that directly or indirectly depend on a HDL file in the common_pkg library.

If a new VHDL file is added to a library or if a filename is changed, then it is necessary to rerun the modelsim_config and to do 'mk clean' on that library. It is important that Modelsim does not have that library open already, so either quit Modelsim first or 'lp' to another library first, before running modelsim_config. Typically it is not necessary to do 'mk clean all', nor is it necessary to delete the entire $HDL_BUILD_DIR library subdirectories.

### 3.3 Simulating the HDL

The simulation is done using a VHDL test bench (tb). These test bench files are made more recognizable by using the 'tb_' prefix. All HDL files that are listed at the test_bench_files key in the hdllib configuration file will have a simulation configuration icon in the Modelsim GUI. To simulate the tb_repack_data.vhd test bench in the dp_repack_data library do e.g.:
```
  ModelSim> lp dp_repack_data
  # Now manually load simulation by double clicking the tb_repack_data icon in the Modelsim Project window
```
Add all signals of 10 levels deep into of test bench hierarchy to the Wave Window:
```
  ModelSim> as 10
```
Run all until the test bench is done:
```
  ModelSim> run -a 
```
This tb_dp_repack_data.vhd test bench is self checking and self stopping. In the Wave window the signals can be observed visually, see Figure 1.


![Figure 1](./wave_tb_dp_repack_data.jpg "wave_tb_dp_repack_data.jpg")  
Figure 1: Modelsim wave window with hierarchical signals view using 'as' command

### 3.4 Developing the HDL

Edit one of the VHDL source files or for example do:
```
  > touch $QS_DIR/cores/dp/dp_pkg/dp_stream_stimuli.vhd
```
Then in Modelsim GUI:
```
  ModelSim> mk all
  ModelSim> restart -f
  ModelSim> run -a
```
The 'mk all' will only compile the touched files and the files that depend on it. The 'restart -f' cleans the wave window but preserves the viewed signal. The 'run -a' runs the simulation.

The tb in the dp_repack_data library run as long as needed to apply the stimuli, and they are self checking. The tb_repack_data is instantiated into a multi test bench tb_tb_repack_data. By running this tb_tb_repack_data the entire dp_repack_data library gets verified in one simulation.

To change project library using ```lp <lib name>``` first the current active simulation needs to be closed using 'quit -sim'. To quit Modelsim entirely, without having to acknowledge the pop up do 'quit -f'.

---
## 4 Appendix: configuration files

### 4.1 hdl_tool_modelsim.cfg

This appendix describes the keys that are used in the hdl_tool_modelsim configuration file that is located in $RADIOHDL_GEAR/config.

***model_tech_altera_lib***  
Defines the path to where the compiled component libraries of a certain Quartus version are stored.


### 4.2 hdllib.cfg keys for Modelsim

This appendix describes the keys that are used in the [modelsim_project_file] section of the hdllib.cfg configuration file. Please see [2] for hdllib.cfg configuration file examples that use these Modelsim specific keys.

***modelsim_copy_files***  
Copy listed all directories and files for simulation with Modelsim, used when tool_name_sim = modelsim in ```hdltool_<toolset>.cfg```. Can be used to eg. copy wave.do or data files from the source directory to the build directory where the Modelsim project file is. For data files that are read in VHDL the path within the build directory then becomes e.g. ```data/<file_name>```.

***modelsim_compile_ip_files***  
This key lists one or more TCL scripts that are executed by the Modelsim mpf before it compiles the rest of the source code. E.g.:

- compile_ip.tcl : a TCL script that contains external IP sources that are fixed and need to be compiled before the synth_files. For the Altera IP the compile_ip.tcl is derived from the msim_setup.tcl that is generated by the MegaWizard or Qsys [2].
- copy_hex_files.tcl : a TCL script that is derived fromt yhe msim_setup.tcl and is used to copy ROM/RAM files initialization files from the generated IP directory to the simulation work directory.
- map_ip.tcl : a TCL script that maps a VHDL library name to another location (not use so far).

---
## 5 Appendix: hdl_tool_modelsim.cfg

Tool configuration file for Mentor/Modelsim from $RADIOHDL_GEAR/config.
```
# configuration file for defining the model installation on this system
model_tech_altera_lib     = ${MODELSIM_ALTERA_LIBS_DIR}/${SYNTH_TOOL_VERSION}
```


