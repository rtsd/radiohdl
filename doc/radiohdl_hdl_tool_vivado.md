# RadioHDL tool settings for Vivado

---
#### Document history:
|Revision|Date|Author|Affiliation|Modification|
|:---|:---|:---|:---|:---|
|1.0|27 feb 2023|R. van der Walle|ASTRON |Created.|

---
#### Contents:
1   Introduction  
1.1 Preconditions  
1.2 Overview  
2   Appendix:configuration files  
2.1 hdl_tool_vivado.cfg  
2.2 hdllib.cfg keys for Vivado  
3   Appendix: hdl_tool_vivado.cfg

---
#### Terminology:
- HDL = Hardware Description Language
- TCL = Tool Command Language

---  
#### References:

[1] radiohdl_user_guide.md  

---  
## 1 Introduction

The RadioHDL package consists of a set of scripts that interpret configuration files and that setup or run other tools for HDL code development. This document describes how to use the RadioHDL command line tools [1] with the vendor tools from Xilinx.

### 1.1 Preconditions

Setup the RadioHDL environment as explained in [1]. The RadioHDL for Vivado is configured using a ```hdl_tool_<tool_name>.cfg``` configuration = hdl_tool_vivado.cfg file, where the tool_name = vivado is defined in a ```hdl_buildset_<buildset_name>.cfg``` configuration file. The hdl_tool_vivado.cfg is described in appendix 2.

### 1.2 Overview

The following RadioHDL tools are about preparing intermediate targets like project files and compiled or pre-generated IP:

* **vivado_config** to create Vivado TCL files for all HDL libraries with a hdllib.cfg. All HDL libraries with source-code that can be synthesized will get two TCL files. One to add the library to a Vivado project, the another to add all depending libraries.

---
## 2 Appendix: configuration files

### 2.1 hdl_tool_vivado.cfg
Not yet implemented.


### 2.2 hdllib.cfg keys for Vivado
***vivado_copy_files***  
Copy listed all directories and files for synthesis with Vivado, used when tool_name_synth = vivado in ```hdl_buildset_<buildset_name>.cfg```. Can be used to e.g. copy Project or bd file from the hdllib.cfg location directory to the build directory.

---
## 3 Appendix: hdl_tool_vivado.cfg

Tool configuration file for Xilinx Vivado from $RADIOHDL_GEAR/config.
```
# configuration file for defining the vivado installation on this system
Not yet implemented.

# extension to the PATH variable
Not yet implemented.

```


