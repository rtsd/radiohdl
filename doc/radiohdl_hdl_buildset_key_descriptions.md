# RadioHDL buildset keys

---
#### Document history:
|Revision|Date|Author|Affiliation|Modification|
|:---|:---|:---|:---|:---|
|1.0|27 jan 2020|E. Kooistra|ASTRON|Created, using text from hdltool_readme.txt.|

---
#### Contents:
1 Introduction  
2 hdl_buildset keys  
2.1 modelsim  
2.2 quartus  

---
#### References:
[1] radiohdl_user_guide.md  
[2] radiohdl_hdl_tool_modelsim.md  
[3] radiohdl_hdl_tool_quartus.md  

---
## 1 Introduction
Each HDL project in RadioHDL has a ```hdl_buildset_<buildset_name>.cfg``` configuration file [1]. This document describes the keys that are used in the ```hdl_buildset_<buildset_name>``` configuration files.

The hdl_buildset configuration file can be divided into sections to group the keys that are used with a specific tool. The sections headers are identified by a [tool_name] that is defined by the *sim_tool_name* key or the *synth_tool_name* key. The [tool_name] refers to a HDL tool configuration file e.g. [2], [3].

---
## 2 hdl_buildset keys

***buildset_name***  
Free to choose name that identifies this buildset. The name is used in the directory structure that is created during the several build/compile stages. It turns out the be handy to make this value equal to the name you use in the name of your ```hdl_buildset_<buildset_name>.cfg``` file.

***technology_names***  
The technology_names key lists the IP technologies that are supported within this buildset_name, eg. ip_stratixiv, ip_arria10. It is possible to define multiple technologies. As long as your tool (e.g. Modelsim) supports these technolgies (i.e. the technology libraries are available in Modelsim) then unused technologies are not disturbing and then they also do not cause simulation load errors with the 'modelsim_search_libraries' key. RadioHDL will only build libs that are based on the technologies you define in this key. Hence the HDL libraries with a hdl_lib_technology key value that is not in the list of technology_names will not be in the build.

***family_names***  
Lists the family(s) your FPGA belongs to. For simulation only the libs for these families are compiled.

***block_design_names***  
Names of tools that can be used for creating block designs, e.g. for Altera this can be 'sopc', 'qsys'.

***project_dir_depth_sim***  
The project file will be located in the build dir or at some levels deeper in the build dir. These optional extra subdirectory levels allow for relative file reference from project file location. This can be useful to be able to keep memory initialisation files in the library build directory that are referred to using some fixed ../../ path in the HDL code. This was used to support a Xilinx ISE project in hdl_buildset_rsp.cfg, which used a binary project file that contained fixed relative paths to higher directories.
- project_deeper_subdir = '' when project_dir_depth_sim = 0 (default),
- project_deeper_subdir = 'p/' when project_dir_depth_sim = 1,
- project_deeper_subdir = 'p/p/' when project_dir_depth_sim = 2,
- project_deeper_subdir = 'p/p/p/' when project_dir_depth_sim = 3, etc

***project_dir_depth_synth***  
Same purpose as project_dir_depth_sim, but for synthesis project file location in build tree.

***sim_tool_name*** and ***sim_tool_version***  
Name and version of simulation tool to use with this buildset. Used as subdirectory name in the build directory, e.g. modelsim, 6.6c, 10.4.Note: RadioHDL will look for a key in this config file that has the name of the value of sim_tool_name extended with '_dir'. So in this example file RadioHDL expects a key with the name modelsim_dir.
  
***synth_tool_name*** and ***synth_tool_version***  
Name and version of synthesis tool to use with this buildset. Used as subdirectory name in the build directory, e.g. quartus, 11.1. Note: RadioHDL will look for a key in this config file that has the name of the value of sim_tool_name extended with '_dir'. So in this example file RadioHDL expects a key with the name quartus_dir.

***lib_root_dirs***  
List of one or more root directories from where all HDL library configuration files (hdllib.cfg) are searched. There is only a single root directory for the build tree, that is defined by $HDL_BUILD_DIR.

### 2.1 [modelsim]
***modelsim_dir***  
Path to installed version of Modelsim. Used to get the location of the modelsim.ini file, e.g. ```${MENTOR_DIR}/<sim_tool_version>/modeltech```. The modelsim.ini needs to be included in the mpf, because without the IEEE and STD libraries are not known anymore after the mpf is loaded.

***modelsim_platform***  
Targeted platform subdirectory name for Modelsim. Executables like vsim, vlog and other are located here.

***modelsim_search_libraries***  
List of IP technology search libraries that will be put in the -L {} option of a Modelsim simulation configuration in the mpf. This avoids that all IP technology needs to be compiled into the work library. The -L {} option is needed for simulations in libraries that use generated IP like ip_stratixiv_phy_xaui which do not recognize the IP technology libraries mapping in [libraries] section in the mpf. The -L {} option is added to all simulation configurations in all mpf even if they do not need it, which is fine because it does not harm and avoids the need for having to decide whether to include it or not per individual library or even per individual simulation configuration.

### 2.2 [quartus]
***quartus_dir***  
Path to installated version of Quartus, e.g. ```${ALTERA_DIR}/<synth_tool_version>```.

