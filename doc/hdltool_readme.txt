Author E. Kooistra, ASTRON:
This hdltool_readme.txt is a stripped down with left over content. All removed
content is now moved and restructured in the RadioHDL markdown (*.md)
documentation.

Contents:

1) Introduction

2) Tool environment setup
  l) UniBoard2 device family
  m) Upgrading the IP for new version of Quartus or for another device family
  
3) HDL environment configuration files
  b) Target configuration scripts
  c) hdl_buildset_<buildset_name>.cfg key sections
  
9) RadioHDL directory structure
  a) applications
  b) boards
  c) libraries
  d) software
  e) tools  
  f) sub directories
  
100) To do
  b) Generate Quartus IP key
  f) Improve support IP for multiple FPGA device types and Quartus tool versions
  g) Improve buildset_name scheme
  h) Declare IP libraries to ensure default binding in simulation.
  
101) More ideas
  a) zip scripts
  b) support dynamic generation of IP
  c) Link RadioHDL developments with the OneClick MyHDL developments.
  
102) Know errors


2) Tool environment setup
l) UniBoard2 device family

The device family of the Arria10 on UniBoard2 v0 is 10AX115U4F45I3SGES as can be seen at the photo of the FPGA:

    $RADIOHDL/boards/uniboard2/libraries/unb2_board/quartus/unb2_board_v0_device_family.JPG

The device family is used in:
- $RADIOHDL/boards/uniboard2/libraries/unb2_board/quartus/unb2_board.qsf
- QSYS IP component files
- QSYS system design file


m) Upgrading the IP for new version of Quartus or for another device family

On 20 May 2015 Eric manually upgraded all current Arria10 IP to Quartus 15.0 and used the Qsys GUI menu view->device family to set the FPGA that is on UniBoard2 v0. 
These are the current Qsys IP components for ip_arria10:

    kooistra@dop233 ip_arria10 $ svn status -q
    M       ddio/ip_arria10_ddio_in_1.qsys
    M       ddio/ip_arria10_ddio_out_1.qsys
    M       ddr4_4g_1600/ip_arria10_ddr4_4g_1600.qsys
    M       ddr4_8g_2400/ip_arria10_ddr4_8g_2400.qsys
    M       fifo/ip_arria10_fifo_dc.qsys
    M       fifo/ip_arria10_fifo_dc_mixed_widths.qsys
    M       fifo/ip_arria10_fifo_sc.qsys
    M       flash/asmi_parallel/ip_arria10_asmi_parallel.qsys
    M       flash/remote_update/ip_arria10_remote_update.qsys
    M       mac_10g/ip_arria10_mac_10g.qsys
    M       phy_10gbase_r/ip_arria10_phy_10gbase_r.qsys
    M       phy_10gbase_r_24/ip_arria10_phy_10gbase_r_24.qsys
    M       pll_clk125/ip_arria10_pll_clk125.qsys
    M       pll_clk200/ip_arria10_pll_clk200.qsys
    M       pll_clk25/ip_arria10_pll_clk25.qsys
    M       pll_xgmii_mac_clocks/ip_arria10_pll_xgmii_mac_clocks.qsys
    M       pll_xgmii_mac_clocks/pll_xgmii_mac_clocks.qsys
    M       ram/ip_arria10_ram_cr_cw.qsys
    M       ram/ip_arria10_ram_crw_crw.qsys
    M       ram/ip_arria10_ram_crwk_crw.qsys
    M       ram/ip_arria10_ram_r_w.qsys
    M       transceiver_phy_1/transceiver_phy_1.qsys
    M       transceiver_phy_48/transceiver_phy_48.qsys
    M       transceiver_pll/transceiver_pll.qsys
    M       transceiver_pll_10g/ip_arria10_transceiver_pll_10g.qsys
    M       transceiver_reset_controller_1/ip_arria10_transceiver_reset_controller_1.qsys
    M       transceiver_reset_controller_1/transceiver_reset_controller_1.qsys
    M       transceiver_reset_controller_24/ip_arria10_transceiver_reset_controller_24.qsys
    M       transceiver_reset_controller_48/ip_arria10_transceiver_reset_controller_48.qsys
    M       transceiver_reset_controller_48/transceiver_reset_controller_48.qsys
    M       tse_sgmii_gx/ip_arria10_tse_sgmii_gx.qsys
    M       tse_sgmii_lvds/ip_arria10_tse_sgmii_lvds.qsys

In addition several other files need to be modified to be able to simulate the IP:

    - the tech_*.vhd files that instantiate the IP component need to have the LIBRARY clause for binding
    - the compile_ip.tcl files need to be updated according to the generated/sim/mentor/msim_setup.tcl
      because the IP library name and file names may have changed
    . the copy_hex_files.tcl files need to be updated 
    . the IP library name in the hdllib.cfg of the IP needs to be changed

This concerned these files:

    kooistra@dop233 trunk $ svn status -q
    M       libraries/technology/10gbase_r/tech_10gbase_r_arria10.vhd
    M       libraries/technology/ddr/tech_ddr_arria10.vhd
    M       libraries/technology/flash/tech_flash_asmi_parallel.vhd
    M       libraries/technology/flash/tech_flash_remote_update.vhd
    M       libraries/technology/ip_arria10/ddio/compile_ip.tcl
    M       libraries/technology/ip_arria10/ddr4_4g_1600/compile_ip.tcl
    M       libraries/technology/ip_arria10/ddr4_4g_1600/copy_hex_files.tcl
    M       libraries/technology/ip_arria10/ddr4_4g_1600/hdllib.cfg
    M       libraries/technology/ip_arria10/ddr4_8g_2400/compile_ip.tcl
    M       libraries/technology/ip_arria10/ddr4_8g_2400/copy_hex_files.tcl
    M       libraries/technology/ip_arria10/ddr4_8g_2400/hdllib.cfg
    M       libraries/technology/ip_arria10/flash/asmi_parallel/compile_ip.tcl
    M       libraries/technology/ip_arria10/flash/asmi_parallel/hdllib.cfg
    M       libraries/technology/ip_arria10/flash/remote_update/compile_ip.tcl
    M       libraries/technology/ip_arria10/flash/remote_update/hdllib.cfg
    M       libraries/technology/ip_arria10/mac_10g/compile_ip.tcl
    M       libraries/technology/ip_arria10/mac_10g/hdllib.cfg
    M       libraries/technology/ip_arria10/phy_10gbase_r/compile_ip.tcl
    M       libraries/technology/ip_arria10/phy_10gbase_r/hdllib.cfg
    M       libraries/technology/ip_arria10/phy_10gbase_r_24/compile_ip.tcl
    M       libraries/technology/ip_arria10/phy_10gbase_r_24/hdllib.cfg
    M       libraries/technology/ip_arria10/pll_clk125/compile_ip.tcl
    M       libraries/technology/ip_arria10/pll_clk125/hdllib.cfg
    M       libraries/technology/ip_arria10/pll_clk200/compile_ip.tcl
    M       libraries/technology/ip_arria10/pll_clk200/hdllib.cfg
    M       libraries/technology/ip_arria10/pll_clk25/compile_ip.tcl
    M       libraries/technology/ip_arria10/pll_clk25/hdllib.cfg
    M       libraries/technology/ip_arria10/pll_xgmii_mac_clocks/compile_ip.tcl
    M       libraries/technology/ip_arria10/pll_xgmii_mac_clocks/hdllib.cfg
    M       libraries/technology/ip_arria10/transceiver_pll_10g/compile_ip.tcl
    M       libraries/technology/ip_arria10/transceiver_pll_10g/hdllib.cfg
    M       libraries/technology/ip_arria10/transceiver_reset_controller_1/compile_ip.tcl
    M       libraries/technology/ip_arria10/transceiver_reset_controller_1/hdllib.cfg
    M       libraries/technology/ip_arria10/transceiver_reset_controller_24/compile_ip.tcl
    M       libraries/technology/ip_arria10/transceiver_reset_controller_24/hdllib.cfg
    M       libraries/technology/ip_arria10/tse_sgmii_gx/compile_ip.tcl
    M       libraries/technology/ip_arria10/tse_sgmii_gx/hdllib.cfg
    M       libraries/technology/ip_arria10/tse_sgmii_lvds/compile_ip.tcl
    M       libraries/technology/ip_arria10/tse_sgmii_lvds/hdllib.cfg
    M       libraries/technology/mac_10g/tech_mac_10g_arria10.vhd
    M       libraries/technology/pll/tech_pll_clk125.vhd
    M       libraries/technology/pll/tech_pll_clk200.vhd
    M       libraries/technology/pll/tech_pll_clk25.vhd
    M       libraries/technology/pll/tech_pll_xgmii_mac_clocks.vhd
    M       libraries/technology/technology_select_pkg.vhd
    M       libraries/technology/tse/tech_tse_arria10.vhd
    
Then run generate-all-ip.sh and try to simulate the test benches that verify the IP:

  - tb_eth
  - tb_tr_10GbE
  - tb_io_ddr
  
and the try to simulate a design, eg.:

  - unb2_minimal
  


3) HDL environment configuration files

b) Target configuration scripts

  t3. verify VHDL test benches in simulation - modelsim_regression_test_vhdl.py

c) Replace SOPC avs2_eth_coe instance
   
Replace in SOPC instance avs_eth_0 with avs_eth_coe by avs2_eth_coe. The eth now uses eth_pkg.vhd from
$RADIOHDL. The avs_eth_coe still uses the eth_pkg.vhd from $UNB and this causes duplicate source file
error in synthesis. Therefore open the SOPC GUI and replace the instance avs_eth_0 with avs_eth_coe by
the avs2_eth_coe component.
   
Make sure that the user_component.ipx is set up oke (see point 2e), because that is needed for SOPC to
find the new avs2_eth_coe in $RADIOHDL.

                
9) RadioHDL directory structure

Currently, the RadioHDL SVN repository is contained within the UniBoard_FP7 SVN repository, at the following URL:

https://svn.astron.nl/UniBoard_FP7/RadioHDL/trunk

The above location might change in the future.

The following sections describe the subdirectories that exist.

a) applications/<project_name>/designs/<design_name>/revisions/<design_name_rev_name>
                                                    /quartus
                                                    /src
                                                    /tb
                               libraries/
                               
   . Contains firmware applications designs, categorized by project.

b) boards/<board_name>
         /uniboard2a/designs/unb2a_led/quartus
                                       src
                                       tb
                    /libraries/unb2a_board/quartus
                                           src
                                           tb

   . Contains board-specific support files and reference/testing designs
     . <board_name>/designs/
       . Contains application designs that can be run on that board to test board-specific features.
     . <board_name>/libraries/
       . Contains board-specific support files, such as firmware modules to communicate with board-specific ICs,
         constraint files, pinning files, board settings template files

c) libraries/<library_category>/<library_name>
             base
             dsp
             external
             io
             technology/...
                        ip_arria10_e3sge3
   . See libraries_hierarchy_and_structure.jpg and readme_libraries.txt
   . Library of reusable firmware blocks, categorized by function and in which generic functionality is separated 
     from technology. Within technology another seperation exists between generic technology and hardware-specific IP.
   . The library_category external/ contains HDL code that was obtained from external parties (e.g. open source).
   . <library_category>/<library_name>/designs/
     . Contains reference designs to synthesize the library block for specific boards.

d) software/
   . Intended for software that runs on a PC, such as control/monitoring of boards and programs to capture and process
     board output, e.g. sent via Ethernet to the processing machine.

e) tools/
   . Contains the RadioHDL tools that are described in this readme file.

f) sub directories
The subdirectories that reoccur contain:

   - src/vhdl  : contains vhdl source code that can be synthesised
   - tb/vhdl   : contains vhdl source code that can is only for simulation (e.g. test benches, models, stubs)
   - quartus   : synthesis specific settings for design that uses Quartus and an Altera FPGA
   - vivado    : synthesis specific settings for design that uses Vivado and an Xilinx FPGA
   - revisions : contains revisions of a design that only differ in generic settign

The separation of src/vhdl and tb/vhdl VHDL files is not mandatory, but can be convenient. An alternative
would be to keep all VHDL in one vhdl/ sub directory. The hdl_lib_uses_synth key in hdllib.cfg typically
contains the files from src/vhdl and the hdl_lib_uses_sim key typically contains the files from tb/vhdl. 
The synthesis will only see the VHDL files that are listed at the hdl_lib_uses_synth key, because the files
at the hdl_lib_uses_sim key are not needed for synthesis and could even confuse synthesis (e.g. warnings that
file IO ignored because it is not possible to synthesize).



100) To do
   
b) Generate Quartus IP key
   The generate_ip.sh scripts for generating the MegaWizard or QSYS IP components in fact are merely a wrapper script
   around the 'qmegawiz' and 'qsys-generate' commands. The generate_ip.sh may seem an unnecessary intermediate step if
   the IP is generated automatically. The IP could be generated automatically based on a megawizard key or a qsys key
   that has the description file as value.
   
   A disadvantage of the generate_ip.sh is that currently they use a fixed selection of the buildset_name, so they do not use
   the -t argument. An advantage of a generate_ip.sh script is that it can hide whether the MegaWizard or QSYS needs
   to be used to generate the IP, so in that way a 'quartus_generate_ip' key can fit both. Another advantage is that
   the generate_ip.sh can call 'qmegawiz' or 'qsys-generate' multiple time and with lots of arguments. Without
   generate_ip.sh this would become:

     quartus_generate_ip = 
       qmegawiz filename and arguments
       qmegawiz filename and arguments
       
   Which is difficult to parse, because arguments can contain spaces and all arguments then have to be on one line. 
   Within the cfg files only lists of values and lists of value pairs without spaces are supported.
   Therefore it is better to define a *_generate_ip key per generate IP tool. This is also more clear, because it shows
   in the cfg file which IP generation tool is used.
     
   The key values are the IP config files that need to be generated. First these IP config files need to be copied
   to the build dir using quartus_copy_files. The IP generate command options are then defined by the method that
   processes the qmegawiz_generate_ip or qsys_generate_ip key. The options can be complicated, but it appears that they
   are the same for all IP, so it is fine to define the options hard coded in the method or by some options txt file:

     qmegawiz_generate_ip = 
       filename
       filename
   
     qsys_generate_ip = filename
      
   The 'quartus_copy_files' key is used to copy the IP generation source file and the generation script to the
   build directory. 
   
   The '*_generate_ip' key identifies the tool script that needs to be ran when the IP has to be generated. The toolscript
   can be a dedicated tool script per key, e.g.:
   
      qmegawiz_generate_ip --> qmegawiz_generate_ip.py
      qsys_generate_ip     --> qsys_generate_ip.py
     
   Eg. a
   --generate_ip command line argument for quartus_config.py (rather than a separate quartus_generate_ip.py script)
   can then generate the IP for all libraries that have such a key. The IP can then be generated outside the SVN tree.
   The $IP_DIR path compile_ip.tcl needs to be adjusted to generated/ and the IP then gets generated in:
   
      $HDL_BUILD_DIR/<buildset_name>/quartus/<hdl_lib_name>/generated
   
   For generated IP that is kept in SVN that IP could still remain there.
   
   The hdllib.cfg should then also define a IP tool name subdirectory in build dir, eg.:
    
     $HDL_BUILD_DIR/<buildset_name>/<tool_name> = $HDL_BUILD_DIR/qsys        or
                                                        $HDL_BUILD_DIR/megawizard
    
   or more general $HDL_BUILD_DIR/ip?
   The $HDL_BUILD_DIR now has a modelsim and quartus subdir:
    
      $HDL_BUILD_DIR/<buildset_name>/modelsim       -- made by modelsim_config.py using sim_tool_name from hdl_buildset_<buildset_name>.cfg
      $HDL_BUILD_DIR/<buildset_name>/quartus        -- made by quartus_config.py using synth_tool_name from hdl_buildset_<buildset_name>.cfg
   
   The IP can be put in a subdir using eg 'tool_name_ip' = quartus_ip:
   
      $HDL_BUILD_DIR/<buildset_name>/quartus_ip     -- made by quartus_config.py using a new tool_name_ip from hdl_buildset_<buildset_name>.cfg
      
   or can it be put in the synth_tool_name directory:
   
      $HDL_BUILD_DIR/<buildset_name>/quartus
      
   or do we need tool_name_megawizard and tool_name_qsys to be able to create:
                                      
      $HDL_BUILD_DIR/<buildset_name>/<tool_name>
      $HDL_BUILD_DIR/unb1/megawizard     -- Altera MegaWizard
      $HDL_BUILD_DIR/unb1/qsys           -- Altera QSYS
      $HDL_BUILD_DIR/unb1/coregen        -- Xilinx
      
   Probably it is not so important whether the IP is generated by MegaWizard or Qsys, because that selection is
   already covered by the generate_ip.sh scripts. In the hdl_buildset_<buildset_name>.cfg both MegaWizard and Qsys can be regarded as
   being part of the Quartus tool. Therefore using tool_name_ip provides sufficient distinction in IP build
   sub directory. However the IP could also be generated into the tool_name_synth build directory and then even
   the tool_name_ip key is not needed, because the tool_name_synth sub directory also suits the Quartus IP
   generation.

   Assume that per IP HDL library there can only be one IP tool that generates the IP. This then implies that
   it is not necessary to distinghuis between megawizard and qsys per HDL library.
   
   Conclusion:
   - Using synth_tool_name = quartus is also sufficient/suitable to define the build subdirectory for IP generation.
   - Use dedicated key per IP generation tool:
     . qmegawiz_generate_ip  --> generate in <synth_tool_name>/<hdl_lib_name>/<output dir>
     . qsys_generate_ip      --> generate in <synth_tool_name>/<hdl_lib_name>/<output dir>
     so assume that multiple generated IP they can share the generated directory, this <output dir> is defined as an 
     option in the method that treats the *_generate_ip key, so it could be different per IP generation tool.
   - Use --generate_ip option with quartus_config.py to generate the IP. This option looks for the *_generate_ip keys
     and then executes them. Default this option is not used, because it the IP only needs t obe generated once.
   - Generated IP that is kept in SVN could be copied to the build dir, and then treated as if it was generated.
   - The sim files of the generated IP are used by Modelsim as well, via compile_ip.tcl, it is oke that these
     generated IP sim files are in the synth_tool_name dir.
   - Having a dedicate tool_name_ip could be nice, to more clearly see in the build tree which libraries have IP and
     to bring them at a the same level as sim_tool_name and synth_tool_name.
     
   
f) Improve support IP for multiple FPGA device types and Quartus tool versions

The IP is FPGA type specific (because it needs to be defined in the Qsys source file) and tool version specific
(because some parameters and even port IO may change). Currently there is only one IP directory per FPGA
technology (eg. ip_arria10) so there is no further separation into device family type and tool version. The
disadvantage of this scheme is that only one version of Quartus can be supported. For a minor version 
change it may not be necessary to upgrade, but for a major version change or for a device family type (eg. from
engineering sample to production sample) change it probably is. To preserve the old version IP it is best to
treat the both the FPGA device version id and the Quartus tool version as a new technology. For example for
Arria10 we now use Quartus 15.0 and device family of UniBoard2 v0 and the IP for that is kept in:

  $RADIOHDL/libraries/technology/ip_arria10/
  
This can be renamed in:

  $RADIOHDL/libraries/technology/ip_arria10_device_10AX115U4F45I3SGES_quartus_15.0/
  
For a directory name it is allowed to use a '.' instead of a '_'. The directory name is not mandatory, but the name convention is
to define the FPGA technology as a triplet:

  ip_<fpga family>_device_<fpga identifier>_quartus_<version>
  
A future version of the IP can be kept in:

  $RADIOHDL/libraries/technology/ip_arria10_device_10AX115U4F45I3SGES_quartus_16.0/

The technology_pkg.vhd then gets;

  c_tech_arria10_device_10AX115U4F45I3SGES_quartus_14_1 = ...;
  c_tech_arria10_device_10AX115U4F45I3SGES_quartus_15_0 = ...;
  c_tech_arria10                                        = c_tech_arria10_device_10AX115U4F45I3SGES_quartus_15_0;  -- optional default
  
The hdllib.cfg of the specific technology IP library then has key (only one value):

  hdl_lib_technology = ip_arria10_device_10AX115U4F45I3SGES_quartus_15_0
  
The hdl_buildset_<buildset_name>.cfg can support multiple technologies eg. to be able to simulate a system with more than one FPGA that are
of different technology (eg. an application with Uniboard1 and Uniboard2):

  technology_names = ip_stratixiv
                     ip_arria10_device_10AX115U4F45I3SGES_quartus_15_0

All libraries that have hdl_lib_technology value that is not in the list of technology_names are removed from the dictionary list
by hdl_config.py, so these IP libraries will not be build.

The build directory currently contains:

  $HDL_BUILD_DIR/<buildset_name>/<tool_name>/<hdl_lib_name>

This scheme is probably still sufficent to also support the FPGA technology as a triplet. However it may be necessary to rename the
library key values in the IP hdllib.cfg to contain the full triplet information, so eg.

  hdl_lib_name = ip_arria10_fifo
  hdl_library_clause_name = ip_arria10_fifo_lib
  
then becomes:

  hdl_lib_name = ip_arria10_device_10AX115U4F45I3SGES_quartus_15_0_fifo
  hdl_library_clause_name = ip_arria10_device_10AX115U4F45I3SGES_quartus_15_0_fifo_lib
  
this is a bit awkward. If only one Quartus version and only one device type are supported per buildset_name, then all these versions can keep 
the same basic hdl_lib_name and hdl_library_clause_name because the IP libraries that are not used can be removed from the build.
Alternatively the hdllib_config.py could support multiple technology version IP libraries that use the same logical library name and use
clause.

The purpose is to be able to handle in parallel different FPGA vendors, different FPGA types and different tool version. We do not have
to support all combinations, but only the combinations that we actually use. Eg. for the FPGA type this implies that we only support the FPGA types
that are actually used on our board. If we make a new board with another FPGA, then we add the technology triplet for that FPGA.


g) Improve buildset_name scheme

The buildset_name defines the combination of Modelsim version and Quartus version. Currently there are buildset_names 'unb1', 'unb2' and 'unb2a'. This
buildset_name scheme can be improved because:

- the buildset_names are tight to a board name 'unb1' (is that oke?) or should we use more general buildset_name names, or do we need a symbolic
  buildset_name names at all? 
- there is also a 'site' level in the bash scripts set_modelsim and set quartus (is that still needed?)


h) Declare IP libraries to ensure default binding in simulation.

Currently the IP library is declared in the technology VHDL file e.g. like 'LIBRARY ip_arria10_ddr4_4g_1600_altera_emif_150;' in tech_ddr_arria10.vhd.
This IP library clause is ignored by synthesis. The IP library must be mapped for simulation, because otherwise Modelsim gives
an error when it compiles the VHDL. Therefore the IP library can then not be excluded for simulation with 'hdl_lib_include_ip' key.
Alternatively the LIBRARY clause could be omitted if the IP library is added to the -L libraries search list of each simulation configuration the
Modelsim project file. This can be achieved adding the IP library to the modelsim_search_libraries key in the hdl_buildset_unb2.cfg. However the problem is
then that if the IP library is not mapped to a directory then Modelsim will issue an error when it tries to search it.
--> For now keep the 'hdl_lib_include_ip' but only use it for synthesis. For simulation the 'hdl_lib_include_ip' is ignored. Which is fine because
    for simulation there is no need to exclude IP libraries.
    
101) More ideas

a) zip scripts
   A zip script can gather all sources that are needed for a particular RadioHDL view point, eg.
   
   - zip all required libraries for a certain level library --> useful for somebody who wants to reuse a HDL library.
   - zip all code necessary to run Python test cases on HW target --> useful for somebody who only wants to use the HW.
   - zip all tool environent code --> useful for somebody who wants to use our tool flow but not our HDL.
   
   Related to this is (how) can we more clearly divide up the RadioHDL/ directory to eg. reuse only parts of it and
   to develop these in other locations/repositories (eg. GIT). Eg. the applications/ directory may not be needed or
   even suitable in RadioHDL/ because applications could be kept elsewhere, even in another repository at another 
   institute.
   
b) support dynamic generation of IP
   Very preliminary ideas:
   Currently the MegaWizard or QSYS component description file is fixed and created manually in advance via the 
   GUI. In future the component description file could be created based on parameters that are defined in the
   hdllib.cfg or even parameters that depend on the requirements from the design. In a dynamic flow the hdllib.cfg
   for IP could even not exist as a file, but only as a dictionary in the script. 
   
c) Link RadioHDL developments with the OneClick MyHDL developments.
   The hdllib.cfg dictionary format seems useful also in the OneClick flow. For some created libraries the hdllib.cfg
   may not exist as a file and but only as the dictionary in the script. The various methods in modelsim_config.py
   and quartus_config.py can also be reused in a OneClick flow.
 
   
102) Know errors

a) ** Fatal: Error occurred in protected context. when loading a simulation in Modelsim
 - Example:
   # Loading ip_stratixiv_phy_xaui_lib.ip_stratixiv_phy_xaui_0(rtl)
   # ** Fatal: Error occurred in protected context.
   #    Time: 0 fs  Iteration: 0  Instance: /tb_<...>/<hierarchy path to ip>/ip_stratixiv_phy_xaui_0_inst/<protected>/<protected>/<protected>/<protected>/<protected>/<protected> File: nofile
   # FATAL ERROR while loading design
 
   Make sure that the StratixIV IP search libraries are defined by modelsim_search_libraries in the hdl_buildset_<buildset_name>.cfg.

