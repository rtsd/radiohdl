# RadioHDL

The purpose of RadioHDL is to speed up HDL development by providing a uniform and automated way of using tools and building code for FPGAs. The RadioHDL user guide provides the introduction to RadioHDL and a quick start example:

* [radiohdl_user_guide.md](./doc/radiohdl_user_guide.md)
 
The RadioHDL package consists of a set of scripts that interpret configuration files and that setup or run tools. RadioHDL uses three kinds of configuration files to setup your source code and tools:

- ***```hdl_buildset_<buildset_name>.cfg```*** configuration file per FPGA board
- ***```hdl_tool_<tool_name>.cfg```*** configuration file per vendor tool
- ***```hdllib.cfg```*** configuration file per HDL library

A RadioHDL configuration file contains a collection of key-value pairs. The configuration files are described in:

* [radiohdl_hdl_buildset_key_descriptions.md](./doc/radiohdl_hdl_buildset_key_descriptions.md)
* [radiohdl_hdl_library_key_descriptions.md](./doc/radiohdl_hdl_library_key_descriptions.md)

RadioHDL was first applied for an FPGA board called UniBoard1. The buildset description for the UniBoard1 provides a more advanced example, that shows how RadioHDL is used to develop HDL for an FPGA board using Mentor Modelsim for simulation and Intel/Altera Quartus for synthesis.

Currently these vendor tools are supported:

* [radiohdl_hdl_tool_modelsim.md](./doc/radiohdl_hdl_tool_modelsim.md)
* [radiohdl_hdl_tool_quartus.md](./doc/radiohdl_hdl_tool_quartus.md)
* [radiohdl_hdl_tool_vivado.md](./doc/radiohdl_hdl_tool_vivado.md)

Currently these FPGA boards are supported:

* [radiohdl_hdl_buildset_uniboard1.md](./doc/radiohdl_hdl_buildset_uniboard1.md)
* [radiohdl_hdl_buildset_uniboard2.md](./doc/radiohdl_hdl_buildset_uniboard2.md)
* [radiohdl_hdl_buildset_alveo.md](./doc/radiohdl_hdl_buildset_alveo.md)

The RadioHDL user guide describes how support for more FPGA boards and more vendor tools can be added to RadioHDL.

The concepts and working of the RadioHDL scripts are described in the RadioHDL programmer guide:

* [radiohdl_programmer_guide.md](./doc/radiohdl_programmer_guide.md)


If you use (part of) the RadioHDL package please attribute the use as indicated this citation NOTICE:

### [NOTICE](NOTICE.md)

The RadioHDL package is Open Source and available under the following LICENSE:

### [LICENSE](LICENSE.md)

The RadioHDL package was developed at [ASTRON] and used in several projects, but others have contributed and are welcome to contribute as well. The CREDITS lists the contributers of RadioHDL:

### [CREDITS](CREDITS.md)


[ASTRON]:https://www.astron.nl
