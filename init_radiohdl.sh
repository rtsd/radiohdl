#!/usr/bin/env bash 
# set -e
###############################################################################
#
# Copyright (C) 2018 
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################

#
# Initialisation script to setup the environment variables for this branch
#

# Make sure it is sourced and no one accidentally gave the script execution rights and just executes it.
if [[ "$_" == "${0}" ]]; then
    echo "ERROR: Use this command with '. ' or 'source '"
    sleep 1
    return
fi

# check for HDL_WORK variable , if it not exists:
# 1) run ". ./init_hdl.sh" in the hdl source dir instead.
# 2) or set variable manualy

if [[ -z "${HDL_WORK}" ]]; then
    echo "environ variable 'HDL_WORK' not available"
    echo "source 'init_hdl.sh' script in hdl directory instead of this one"
    echo ". ./init_hdl.sh"
    return
fi

# check if the needed environment variables are available
for var in "${ALTERA_DIR}" "${MENTOR_DIR}"
do
    if [[ -z "$var" ]]; then
        echo "not all needed environ variables are available"
        echo "one of: ALTERA_DIR, MENTOR_DIR is missing"
        echo "they should be in your bashrc file pointing to the software install dirs"
        return
    fi
done

echo "RadioHDL environment will be setup for" $HDL_WORK

# Figure out where this script is located and set environment variables accordingly
export RADIOHDL_GEAR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
# setup paths to build and config dir if not already defined by the user.
export RADIOHDL_CONFIG="${RADIOHDL_CONFIG:-${RADIOHDL_GEAR}/config}"
export RADIOHDL_BUILD_RESULT="${HDL_WORK}/build_result"
export RADIOHDL_REGTEST_RESULT="${HDL_WORK}/regtest_result"

if [ ! -d "${RADIOHDL_BUILD_RESULT}" ]; then
    mkdir "${RADIOHDL_BUILD_RESULT}"
fi

if [ ! -d "${RADIOHDL_REGTEST_RESULT}" ]; then
    mkdir -p "${RADIOHDL_REGTEST_RESULT}/unb2b/modelsim"
    mkdir -p "${RADIOHDL_REGTEST_RESULT}/unb2c/modelsim"
fi

. ${RADIOHDL_GEAR}/generic.sh

# Extend the PATH and PYTHONPATH variables
pathadd "PATH" "${RADIOHDL_GEAR}"/quartus "${RADIOHDL_GEAR}"/ise "${RADIOHDL_GEAR}"/modelsim "${RADIOHDL_GEAR}"/core "${RADIOHDL_GEAR}"/regressiontest
pathadd "PYTHONPATH" "${RADIOHDL_GEAR}"/core

unset user_components_file
