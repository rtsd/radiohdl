#!/usr/bin/env python3
###############################################################################
#
# Copyright (C) 2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################
import os
from os.path import expandvars
from argparse import ArgumentParser
from configfile import ConfigFile

if __name__ == '__main__':
    # setup parser and parse the arguments.
    argparser = ArgumentParser(description='Options and arguments for showing hdl_config keys values')
    argparser.add_argument('configfile', help="Filename like 'hdl_buildset_<boardtype>.cfg'")
    argparser.add_argument('keyname', help="Name of the key to show the value of.")
    args = argparser.parse_args()
   
    # resolve full name of configfile and force it to be explicit absolute or relative.
    full_configfile_name = expandvars(args.configfile)
    if full_configfile_name[0] != '/':
        full_configfile_name = "./" + full_configfile_name
    # read the file
    cfg_info = ConfigFile(full_configfile_name)
    cfg_info.resolve_key_references()

    print("{}\n".format(os.path.expandvars(cfg_info.get_value(args.keyname, must_exist=True))))
