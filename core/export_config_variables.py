#!/usr/bin/env python3
###############################################################################
#
# Copyright (C) 2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################
import sys
import os
from os.path import expandvars
from argparse import ArgumentParser
from configfile import ConfigFile

if __name__ == '__main__':
    # setup parser and parse the arguments.
    argparser = ArgumentParser(description="Options and arguments for constructing an 'export' command for shell based on the content of a hdltool config file.")
    argparser.add_argument('configfile', help="Filename like 'hdl_buildset_<boardtype>.cfg'")
    argparser.add_argument('keynames', help="Name(s) of the key(s) to show the value of. Use comma to seperate multiple keys.")
    argparser.add_argument('--varnames', help="Name(s) of the environment variable(s) the keys are mapped to. Default the keynames in capitals are used as environment variable names.")
    argparser.add_argument('--is-group', help="The keynames refer to groups of environment variables that must be set.",
                           action="store_true")
    argparser.add_argument('--optional', help="The keynames are optional. When them do not exist not error is generated",
                           action="store_true")
    args = argparser.parse_args()

    # resolve full name of configfile and force it to be explicit absolute or relative.
    full_configfile_name = expandvars(args.configfile)
    if full_configfile_name[0] != '/':
        full_configfile_name = "./" + full_configfile_name
    # read the file
    cfg_info = ConfigFile(full_configfile_name)
    cfg_info.resolve_key_references()

    # parse the keys if they are no group references
    if not args.is_group:
        # setup key- and variable- names
        keys = args.keynames.split(',')
        if args.varnames:
            env_vars = args.varnames.split(',')
            if len(keys) != len(env_vars):
                argparser.error("Number of variable names must match the number of keys.")
        else:
            env_vars = []
            for key in keys:
                env_vars.append(key.upper())

        # finally construct an export command for the key value pairs.
        for idx, key in enumerate(keys):
            print("export {}='{}'\n".format(env_vars[idx],
                                            os.path.expandvars(cfg_info.get_value(key, must_exist=not(args.optional)))))
        sys.exit(0)

    # Each key contains key-value pairs that must be exported in stead of a value
    if args.varnames:
        argparser.error("The option --varnames can not be used in combination with the option --is-group.")

    # print("args.keynames=%s" % str(args.keynames))
    keys = args.keynames.split(',')
    for key in keys:
        kv_pairs = cfg_info.get_value(key, must_exist=not(args.optional))
        # currently 'by definition' the value we got has the format: <key> <value> [<key> <value> [...]]
        # check we have an even number of items
        # print("KV_pairs={}".format(kv_pairs))
        if kv_pairs is None:
            continue
        items = kv_pairs.split()
        if len(items) % 2:
            argparser.error("Key '{}' should contain an even number of items ({}).".format(key, items))
        for idx in range(0, len(items)//2, 2):
            print("export {}='{}'\n".format(items[idx].upper(), os.path.expandvars(items[idx+1])))

