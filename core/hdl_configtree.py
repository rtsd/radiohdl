###############################################################################
#
# Copyright (C) 2014-2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################

from hdl_configfile import HdlTool, HdlBuildset, HdlLib
from configtree import ConfigTree

__all__ = ['HdlToolTree', 'HdlBuildsetTree', 'HdlLibTree']


class HdlToolTree(ConfigTree):
    """
    Class the represents the content of a set of hdltool_<tool>.cfg configuration files.
    """
    def __init__(self, rootdirs, filename, sections=None):
        """
        Read the hdltool configuration files and stores them in this tree.
        :raise ConfigFileException
        """
        super(HdlToolTree, self).__init__(rootdirs, filename, sections)

    def _factory_constructor(self, full_filename):
        "Function for returning the readin configfile."
        return HdlTool(full_filename)


class HdlBuildsetTree(ConfigTree):
    """
    Class the represents the content of a set of hdl_buildset_<buildset>.cfg configuration files.
    """
    def __init__(self, rootdirs, filename, sections=None):
        """
        Read the hdlbuildset configuration files and stores them in this tree.
        :raise ConfigFileException
        """
        super(HdlBuildsetTree, self).__init__(rootdirs, filename, sections)

    def _factory_constructor(self, full_filename):
        "Function for returning the readin configfile."
        return HdlBuildset(full_filename)


class HdlLibTree(ConfigTree):
    """
    Class the represents the content of a set of hdllib.cfg configuration files.
    """
    def __init__(self, rootdirs, filename, sections=None):
        """
        Reads the hdllib configuration files and stores them in this tree.
        :raise ConfigFileException
        """
        super(HdlLibTree, self).__init__(rootdirs, filename, sections)

    def _factory_constructor(self, full_filename):
        "Function for returning the readin configfile."
        return HdlLib(full_filename)
