###############################################################################
#
# Copyright (C) 2014-2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################
import os
import os.path
import re
from common_radiohdl import listify
from configfile import ConfigFile, ConfigFileExceptionError


class ConfigTree(object):

    def __init__(self, rootdirs, filename, sections=None):
        """
        Collect the information of all configuration files that are present under the rootdirs.
        """
        # Save construction arguments
        self.rootdirs = listify(rootdirs)
        self.filename = filename
        self.sections = listify(sections)

        # Define result variables
        self._configfiles = {}

        # search and read the config files.
        self._read_all_configfiles()
        if len(self._configfiles) == 0:
            raise ConfigFileExceptionError("No '%s' files found in directory tree(s) '%s'." % (filename, rootdirs))

    def _read_all_configfiles(self):
        """
        Recursively search the rootdirs to find the configfiles and add the content to our admin.
        """
        ref_pattern = self.filename.replace("*", "(.+?)")
        name_mask = re.compile(ref_pattern + "$")
        for rootdir in self.rootdirs:
            for root, _, files in os.walk(rootdir):
                for some_filename in files:
                    if name_mask.search(some_filename):
                        cfgfile = self._factory_constructor("{}/{}".format(root, some_filename))
                        # check for duplicates
                        if cfgfile.ID in list(self._configfiles.keys()):
                            raise ConfigFileExceptionError("File with id '%s' found twice (at '%s' and '%s')" % (
                                                           cfgfile.ID, self._configfiles[cfgfile.ID].location,
                                                           cfgfile.location))
                        self._configfiles[cfgfile.ID] = cfgfile

    def _factory_constructor(self, full_filename):
        """
        Function for returning the readin configfile. Derived classes *must* redefine this function.
        """
        return ConfigFile(full_filename, sections=self.sections)

    @property
    def configfiles(self):
        return self._configfiles

    def remove_files_from_tree(self, files_to_remove):
        """
        Remove the given list of configfiles from our configfile administration.
        :raise   KeyError when one of the files does not exist in our admin.
        """
        for cfgfile_id in files_to_remove:
            self._configfiles.pop(cfgfile_id)

    def limit_tree_to(self, files_to_keep):
        """
        Limit the configfile collection in our admin to the ones given in the files_to_keep argument.
        """
        for cfgfile in self._configfiles:
            if cfgfile.ID not in files_to_keep:
                self._configfiles.pop(cfgfile.ID)

    def get_key_values(self, key, configfiles=None, must_exist=False):
        """
        Get the value of a key in all configfiles. If the key does not exist in a configfile and
        the flag must_exist is False then None is added to the result list. When the flag must_exist
        is true and the key is not defined in a configfile then an exception is raised.
        The configfiles to search in may be limited to 'configfiles' otherwise the whole tree is used.
        :return   List of values
        :raises   ConfigFileException
        """
        if configfiles is None:
            configfiles = self._configfiles

        result = []
        for cfgfile in configfiles:
            result.append(cfgfile.get_value(key, must_exist))
        return result

    def get_configfiles(self, key, values=None, user_configfiles=None):
        """
        Get a list with all configfiles that contain the key with a value specified in values.
        If values==None then a list of all configfiles is returned that contain the key.
        The configfiles to search in may be restricted to the user_configfiles.
        """
        file_list = list(self._configfiles.values()) if not user_configfiles else user_configfiles

        result = []
        for cfgfile in file_list:
            if cfgfile not in result and key in cfgfile.content:
                if values is None or cfgfile.content[key] in values:
                    result.append(cfgfile)
        return result
