# ##########################################################################
# Copyright 2020
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . terminal functions
# ##########################################################################

import os
import sys
import subprocess
import signal
import logging
import time
from queue import Queue, Empty
from threading import Thread

PYTHON_VERSION = sys.version_info[0]

logger = logging.getLogger('main.terminal')

def run_cmd(cmd):
    """ run_cmd()
    run 'cmd' in a terminal and return response
    cmd: command to execute in terminal
    return: 'stdout' or 'Error, stderr' in case of a error
    """
    logger.debug("run_cmd() >> %s", cmd)
    # _my_env = os.environ.copy()
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)  #, env=_my_env)
    _stdout, _stderr = proc.communicate()
    if _stderr:
        stderr = _stderr.decode('utf-8') if PYTHON_VERSION == 3 else _stderr
        print(stderr)
        return 'Error, {}'.format(stderr)
    stdout = _stdout.decode('utf-8') if PYTHON_VERSION == 3 else _stdout
    logger.debug("run_cmd() stdout = %s", stdout)
    return stdout

class TimeoutAlarm:
    """
    raise signal on timeout and call a function
    """
    def __init__(self, handler):
        self.timeout = 0.0
        self.handler = handler

    def set_alarm(self, timeout):
        self.timeout = timeout
        if self.timeout > 0.0:
            signal.signal(signal.SIGALRM, self.handler)
            signal.alarm(timeout)  # Set timeout alarm

    def reset_alarm(self):
        if self.timeout > 0.0:
            signal.alarm(0)  # Disable the alarm


class Terminal:
    """ Terminal class
    This class can be used to execute terminal commands.
    using a timeout for maximum duration.
    It will collect stdout, stderr and the exitcode.
    usage example:
      terminal = Terminal(60)  # set default timeout to 60 seconds
      cmd = "ls -ltr"  # cmd to run
      terminal.run_cmd(cmd)  # run command, using default timeout
      terminal.run_cmd(cmd, timeout=10)  # run command, set timeout to 10 seconds
      terminal.run_cmd(cmd, print_output=True)  # run command, and print also to stdout
      
      # get output from the command
      exit_code = terminal.exit_code()
      std_out = terminal.stdout()
      std_err = terminal.stderr()
      timedout = terminal.exit_on_timeout()

    """
    def __init__(self, timeout=None, print_stdout_on_timeout=True, env=None):
        self._pid             = None
        self._cmd             = None
        self._default_timeout = 3600 if timeout is None else timeout
        self._env             = env
        self._kill            = False
        self._timeout_print   = print_stdout_on_timeout
        self._print           = False
        self._stdout          = []
        self._stderr          = []
        self._error           = False
        self._exit_code       = None
        self._exit_on_timeout = False 
        logger.debug('Terminal init done')

    def kill(self):
        self._kill = True

    def get_pid(self):
        return self._pid

    def run_cmd(self, cmd, timeout=None, print_output=None):
        """ run_cmd()
        cmd: command to execute
        return: exit_code from executed command
        """
        self._kill = False
        self._cmd = cmd
        self._timeout = self._default_timeout if timeout is None else timeout
        self._print = False if print_output is None else print_output
        self._exit_code = None
        self._exit_on_timeout = False
        self._stdout = []
        self._stderr = []
        self._error  = False
        logger.debug('run cmd: "%s"', self._cmd)
        self._run_cmd_get_exitcode()
        # print stdout to screen if timed out
        if self._timeout_print and self._exit_on_timeout:
            print(self.stdout())
        return self._exit_code

    def last_cmd(self):
        """ last_cmd()
        return: last executed command
        """
        return self._cmd

    def error(self):
        return self._error

    def no_error(self):
        return not self._error

    def exit_code(self):
        """ exit_code()
        return exit_code from last executed cmd
        """
        return self._exit_code

    def exit_on_timeout(self):
        return self._exit_on_timeout
        
    def stdout(self):
        """ stdout()
        return: response from last command
        """
        return ''.join(self._stdout)

    def stderr(self):
        """ stderr()
        return: error from last command, if no error a empty string is returned
        """
        return '\n'.join(self._stderr) + '\n'

    def _run_cmd_get_exitcode(self):
        """ _run_cmd_get_exitcode)
        run self._cmd in a terminal and save stdout, stderr and returncode from process
        """

        ON_POSIX = 'posix' in sys.builtin_module_names

        def enqueue_output(out, queue):
            for line in iter(out.readline, b''):
                queue.put(line)
            out.close()

        stop_time = time.time() + self._timeout
        # logger.debug("timenow=%3.0f, stoptime=%3.0f", time.time(), stop_time)
        proc = subprocess.Popen(self._cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid, close_fds=ON_POSIX, env=self._env)
        self._pid = proc.pid
        # print(proc.pid)

        stdout_q = Queue()
        stdout_t = Thread(target=enqueue_output, args=(proc.stdout, stdout_q))
        stdout_t.deamon = True
        stdout_t.start()

        stderr_q = Queue()
        stderr_t = Thread(target=enqueue_output, args=(proc.stderr, stderr_q))
        stderr_t.deamon = True
        stderr_t.start()

        # wait until process ends or timed out
        while True:
            try:
                if self._kill:
                    break

                if time.time() >= stop_time:
                    timeout_msg = []
                    timeout_msg.append("*" * (26 + len(self._cmd)) + "\n")
                    timeout_msg.append("* TIMEOUT ERROR on cmd: %s *\n" % self._cmd)
                    timeout_msg.append("*" * (26 + len(self._cmd)) + "\n")
                    self._stdout.extend(timeout_msg)
                    if self._timeout_print:
                        print("".join(timeout_msg))
                    logger.warning("timeout process %s", self._pid)
                    self._exit_on_timeout = True
                    self._kill = True
                    break
                if proc.poll() is not None:
                    break

                try:
                    line = stdout_q.get_nowait()
                    if PYTHON_VERSION == 3:
                        line = line.decode("utf-8")
                    self._stdout.append(line)
                    if self._print:
                        print(line)
                except Empty:
                    pass

                try:
                    line = stderr_q.get_nowait()
                    if PYTHON_VERSION == 3:
                        line = line.decode("utf-8")
                    self._stderr.append(line)
                    if self._print:
                        print(line)
                    self._error = True
                except Empty:
                    pass
            except KeyboardInterrupt:
                self._kill = True

        # readout rest of buffers
        try:
            while True:
                line = stdout_q.get_nowait()
                if PYTHON_VERSION == 3:
                    line = line.decode("utf-8")
                self._stdout.append(line)
                if self._print:
                    print(line)
        except (Empty, KeyboardInterrupt):
            pass

        try:
            while True:
                line = stderr_q.get_nowait()
                if PYTHON_VERSION == 3:
                    line = line.decode("utf-8")
                self._stderr.append(line)
                if self._print:
                    print(line)
                self._error = True
        except (Empty, KeyboardInterrupt):
            pass

        # logger.debug("cmd returncode = %s", str(proc.returncode))
        self._exit_code = proc.returncode

        if self._kill is True:
            logger.debug("kill process group %s", str(self._pid))
            os.killpg(os.getpgid(proc.pid), signal.SIGKILL)
            self._exit_code = -1
