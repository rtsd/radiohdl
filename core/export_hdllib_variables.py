#!/usr/bin/env python3
###############################################################################
#
# Copyright (C) 2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################
import os
from os.path import expandvars
from argparse import ArgumentParser
from hdl_configfile import HdlBuildset
from hdl_configtree import HdlLibTree

if __name__ == '__main__':
    # setup parser and parse the arguments.
    argparser = ArgumentParser(description='Options and arguments for exporting hdllib keys values')
    argparser.add_argument('buildset', help="Filename like 'hdl_buildset_<boardtype>.cfg'")
    argparser.add_argument('libname', help="Name of the library to search in.")
    argparser.add_argument('keys', help="Name of variable(s) to export.")
    args = argparser.parse_args()
  
    # read the buildset file
    full_buildsetfile_name = expandvars("${RADIOHDL_CONFIG}/hdl_buildset_%s.cfg" % (args.buildset))
    buildset_info = HdlBuildset(full_buildsetfile_name)
    buildset_info.resolve_key_references()

    # find out where the hdllibs files are and read them in
    root_dirs = [expandvars(rootdir) for rootdir in buildset_info.lib_root_dirs.replace("\t", " ").split(" ")
                 if rootdir != '']
    lib_tree = HdlLibTree(rootdirs=root_dirs, filename="hdllib.cfg")
    for key in args.keys.split(','):
        print("export {}='{}'\n".format(
              key.lower(),
              os.path.expandvars(lib_tree.configfiles[args.libname].get_value(key=key, must_exist=True))))
