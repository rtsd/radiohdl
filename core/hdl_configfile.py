###############################################################################
#
# Copyright (C) 2014-2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################

from configfile import ConfigFile

__all__ = ['HdlTool', 'HdlBuildset', 'HdlLib']


class HdlTool(ConfigFile):
    """
    Class the represents the content of a hdltool_<tool>.cfg configuration file.
    """
    _HDLTOOL_ATTRIBUTES = []

    def __init__(self, filename, sections=None):
        """
        Read the hdltoolset configuration file and check presence of the required keys.
        :raise ConfigFileException
        """
        super(HdlTool, self).__init__(filename, sections, required_keys=self._HDLTOOL_ATTRIBUTES)


class HdlBuildset(ConfigFile):
    """
    Class the represents the content of a hdl_buildset_<buildset>.cfg configuration file.
    """
    _HDLBUILDSET_ATTRIBUTES = ['buildset_name', 'technology_names', 'family_names', 'block_design_names',
                               'lib_root_dirs', 'sim_tool_name', 'sim_tool_version',
                               'synth_tool_name', 'synth_tool_version']

    def __init__(self, filename, sections=None):
        """
        Read the hdlbuildset configuration file and check presence of the required keys.
        :raise ConfigFileException
        """
        super(HdlBuildset, self).__init__(filename, sections, required_keys=self._HDLBUILDSET_ATTRIBUTES)

    @property
    def ID(self):
        "Returns uniq ID (string) to identify this particular file."
        return self.buildset_name


class HdlLib(ConfigFile):
    """
    Class the represents the content of a hdllib.cfg configuration file.
    """
    _HDLLIB_ATTRIBUTES = ['hdl_lib_name', 'hdl_library_clause_name', 'hdl_lib_uses_synth',
                          'hdl_lib_uses_sim', 'hdl_lib_technology', 'synth_files',
                          'test_bench_files']

    def __init__(self, filename, sections=None):
        """
        Read the hdllib configuration file and check presence of the required keys.
        :raise ConfigFileException
        """
        super(HdlLib, self).__init__(filename, sections, required_keys=self._HDLLIB_ATTRIBUTES)

    @property
    def ID(self):
        "Returns uniq ID (string) to identify this particular file."
        return self.hdl_lib_name
