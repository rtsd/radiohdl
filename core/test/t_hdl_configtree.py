###############################################################################
#
# Copyright 2018 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten
# ASTRON Netherlands Institute for Radio Astronomy
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License. 
#
###############################################################################

import unittest
from configtree import *
from hdl_configtree import *


class Test_construction(unittest.TestCase):
    "Class to the various ways of construction"

    def test_wrong_filename(self):
        "Test constructor with non-existing rootdir"
        self.assertRaises(ConfigFileExceptionError, ConfigTree, "/Is/Not/A/Valid/Directory", "dict.txt")

    def test_empty_dictfile(self):
        "Test constructor with empty config file"
        tree = ConfigTree("./cdf_dir/empty_file", "empty_dict.txt")
        self.assertEqual(len(tree.configfiles), 1)

    def test_comment_only_dictfile(self):
        "Test constructor with comment-only config files"
        tree = ConfigTree("./cdf_dir/empty_file", "comment_only_dict.txt")
        self.assertEqual(len(tree.configfiles), 1)


class Test_tree_behaviour(unittest.TestCase):
    "Class to test the 'tree' functionality of the class"

    def test_tree_with_configfiles(self):
        "Test constructor with a tree with configfiles all containing a 'key_1' label"
        "that holds its relative path in the tree"
        tree = ConfigTree("./cdf_dir/tree/cfgfile", "dict.txt")
        for cfg in list(tree.configfiles.values()):
            # print cfg.ID, cfg.content
            expected_value = cfg.ID.replace("./cdf_dir/tree/cfgfile", "top_dir").replace("/dict.txt", "")
            self.assertEqual(expected_value, cfg.key_1)

    def test_hdllib_tree(self):
        "Test if we can read in a tree with hdllib files."
        tree = HdlLibTree("./cdf_dir/tree/hdllib", "test_hdllib.cfg")
        self.assertEqual(len(tree.configfiles), 2)
        util = tree.configfiles['util']
    self.assertEqual(util.hdl_library_clause_name, 'util_lib')
    self.assertEqual(util.synth_files, 'src/vhdl/util_logic.vhd src/vhdl/util_heater_pkg.vhd src/vhdl/util_heater.vhd')
    technology = tree.configfiles['technology']
    self.assertEqual(technology.hdl_library_clause_name, 'technology_lib')
    self.assertEqual(technology.synth_files, 'technology_pkg.vhd $HDL_BUILD_DIR/<buildset_name>/modelsim/technology/technology_select_pkg.vhd')

    def test_hdlbuildset_tree(self):
        "Test if we can read in a tree with hdlbuildset files."
        tree = HdlBuildsetTree("./cdf_dir/tree/hdlbuildset", "hdl_buildset_*.cfg")
        self.assertEqual(len(tree.configfiles), 2)
        rsp = tree.configfiles['rsp']
    self.assertEqual(rsp.technology_names, 'ip_virtex4')
    self.assertEqual(rsp.model_tech_altera_lib, '/home/software/modelsim_altera_libs/<synth_tool_version>')
    unb1 = tree.configfiles['unb1']
    self.assertEqual(unb1.technology_names, 'ip_stratixiv')
    self.assertEqual(unb1.model_tech_altera_lib, '/home/software/modelsim_altera_libs/<synth_tool_version>')

    def test_hdltool_tree(self):
        "Test if we can read in a tree with hdltool files."
        tree = HdlToolTree("./cdf_dir/tree/hdltool", "hdl_tool_*.cfg")
        self.assertEqual(len(tree.configfiles), 2)
        altera = tree.configfiles['./cdf_dir/tree/hdltool/hdl_tool_altera.cfg']
        self.assertEqual(altera.altera_rootdir, "${ALTERA_DIR}/altera")
        quartus = tree.configfiles['./cdf_dir/tree/hdltool/hdl_tool_quartus.cfg']
        self.assertEqual(quartus.quartus_rootdir, "${QUARTUS_DIR}/quartus")


if __name__ == '__main__':
    unittest.main(verbosity=2)

