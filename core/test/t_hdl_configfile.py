###############################################################################
#
# Copyright 2018 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten
# ASTRON Netherlands Institute for Radio Astronomy
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License. 
#
###############################################################################

import unittest
from configfile import *
from hdl_configfile import *


class Test_construction(unittest.TestCase):
    "Class to the various ways of construction"

    def test_wrong_filename(self):
        "Test constructor with non-existing file"
        self.assertRaises(ConfigFileExceptionError, ConfigFile, "/Is/Not/A/Valid/Directory")

    def test_empty_dictfile(self):
        "Test constructor with empty config file"
        cfg = ConfigFile("./cdf_dir/empty_file/empty_dict.txt")
        self.assertEqual(len(cfg.content), 0)

    def test_comment_only_dictfile(self):
        "Test constructor with comment-only config files"
        cfg = ConfigFile("./cdf_dir/empty_file/comment_only_dict.txt")
        self.assertEqual(len(cfg.content), 0)


class Test_key_value_spacing(unittest.TestCase):
    "Class to the various kind of spacing between the keys and the values"

    def test_key_value_spacing(self):
        cfg = ConfigFile("./cdf_dir/correct_files/key_value_test.txt")
        self.assertEqual(cfg.space_key_1, "value_1")
        self.assertEqual(cfg.space_key_2, "value_2")
        self.assertEqual(cfg.space_key_3, "value_3")
        self.assertEqual(cfg.space_key_4, "value_4")
        self.assertEqual(cfg.space_key_5, "value_5")
        self.assertEqual(cfg.space_key_6, "value_6")
        self.assertEqual(cfg.space_key_7, "value_7")
        self.assertEqual(cfg.multi_key_1, "value10 value11 value12")
        self.assertEqual(cfg.multi_key_2, "value20, value21, value22")
        self.assertEqual(cfg.multi_key_3, "value30 value31 value32 value33")
        self.assertEqual(cfg.multi_key_4, "value40 = value41 = value42")
        self.assertEqual(cfg.tricky_key_1, "")
        self.assertEqual(cfg.tricky_key_2, "tricky_value_2")
        self.assertEqual(cfg.tricky_key_3, "")
        self.assertEqual(cfg.section_headers, ['"my_section"'])
        self.assertEqual(cfg.warning_key_1, "Be aware that multiline values can be tricky:  this also belongs to previous key 'warning_key_1'")
        # also test attribute access versus item access
        self.assertEqual(cfg.multi_key_2, cfg['multi_key_2'])
        print(cfg.content)

    def test_sections(self):
        cfg = ConfigFile("./cdf_dir/correct_files/section_test.txt")
        self.assertEqual(cfg.global_key_1, "global_1"),
        self.assertEqual(cfg.global_key_2, '[ "aap", "noot", "mies" ]'),
        self.assertEqual(cfg.just_some_key_1, "section2 value1"),
        self.assertEqual(cfg.just_some_key_2, "section2 value2"),
        self.assertEqual(cfg.section_headers, ['section_1', 'section_2'])

    def test_dangling_value(self):
        "Test if a value without a key is detected"
        self.assertRaises(ConfigFileExceptionError, ConfigFile, "./cdf_dir/wrong_files/dangling_test.txt")

    def test_keys_with_spaces(self):
        "Test if a key that contains spaces is detected"
        self.assertRaises(ConfigFileExceptionError, ConfigFile, "./cdf_dir/wrong_files/wrong_key_test.txt")


class Test_reference_key_substitution(unittest.TestCase):
    "Class to the the substitution of referenced keys."

    def test_read_the_file(self):
        cfg = ConfigFile("./cdf_dir/referenced_files/reference_test.txt")
        self.assertEqual(cfg.early_ref_key1, "before <ref_key_2> is defined")
        self.assertEqual(cfg.simple_ref_1, "a value with <ref_key_1>")
        self.assertEqual(cfg.double_ref_1, "a value with twice <ref_key_1><ref_key_1>")
        self.assertEqual(cfg.triple_key_1, "its here <ref_key_1> and here <ref_key_1> and here <ref_key_1>!!!")
        self.assertEqual(cfg.triple_key_2, "its here <ref_key_2> and here <ref_key_1>!!!")
        self.assertEqual(cfg.ref_only_key_1, "<ref_key_1>")
        self.assertEqual(cfg.wrong_ref_1, "what will double brackets <<ref_key_2>> do?")
        self.assertEqual(cfg.undefined_key_1, "reference to <non existing key>")

        self.assertEqual(cfg.nested_key_1, "some_value")
        self.assertEqual(cfg.nested_key_2, "<nested_key_1>")
        self.assertEqual(cfg.nested_key_3, "<nested_key_2>")
        self.assertEqual(cfg.reverse_nested_key_1, "<reverse_nested_key_2>")
        self.assertEqual(cfg.reverse_nested_key_2, "<reverse_nested_key_3>")
        self.assertEqual(cfg.reverse_nested_key_3, "some_value")
        self.assertEqual(cfg.mutual_key_1, "<mutual_key_2>")
        self.assertEqual(cfg.mutual_key_2, "<mutual_key_1>")
        self.assertEqual(cfg.loop_key_1, "<loop_key_1>")

        cfg.resolve_key_references()

        self.assertEqual(cfg.early_ref_key1, "before multiple words in the value is defined")
        self.assertEqual(cfg.simple_ref_1, "a value with single_reference")
        self.assertEqual(cfg.double_ref_1, "a value with twice single_referencesingle_reference")
        self.assertEqual(cfg.triple_key_1, "its here single_reference and here single_reference and here single_reference!!!")
        self.assertEqual(cfg.triple_key_2, "its here multiple words in the value and here single_reference!!!")
        self.assertEqual(cfg.ref_only_key_1, "single_reference")
        self.assertEqual(cfg.wrong_ref_1, "what will double brackets <<ref_key_2>> do?")
        self.assertEqual(cfg.undefined_key_1, "reference to <non existing key>")

        self.assertEqual(cfg.nested_key_1, "some_value")
        self.assertEqual(cfg.nested_key_2, "some_value")
        self.assertEqual(cfg.nested_key_3, "some_value")
        self.assertEqual(cfg.reverse_nested_key_1, "<reverse_nested_key_3>")
        self.assertEqual(cfg.reverse_nested_key_2, "some_value")
        self.assertEqual(cfg.reverse_nested_key_3, "some_value")
        self.assertEqual(cfg.mutual_key_1, "<mutual_key_1>")
        self.assertEqual(cfg.mutual_key_2, "<mutual_key_1>")
        self.assertEqual(cfg.loop_key_1, "<loop_key_1>")


class Test_hdltool_file(unittest.TestCase):
    "Class to test the hdltool class."

    def test_read_good_hdltool_file(self):
        tool = HdlTool("./cdf_dir/hdltool_files/hdl_tool_quartus.cfg")
        self.assertEqual(tool.user_environment_variables, "altera_hw_tcl_keep_temp_files	1")


class Test_hdl_buildset_file(unittest.TestCase):
    "Class to test the hdltool class."

    def test_read_good_hdlbuildset_file(self):
        buildset = HdlBuildset("./cdf_dir/hdlbuildset_files/hdl_buildset_unb1.cfg")
        self.assertEqual(buildset.buildset_name, "unb1")
        self.assertEqual(buildset.technology_names, "ip_stratixiv")
        self.assertEqual(buildset.lib_root_dirs, "${HDL_WORK}/libraries ${HDL_WORK}/applications ${HDL_WORK}/boards")

    def test_read_wrong_hdlbuildset_file(self):
        self.assertRaises(ConfigFileExceptionError, HdlBuildset, "./cdf_dir/hdlbuildset_files/hdl_buildset_wrong.cfg")


class Test_hdllib_file(unittest.TestCase):
    "Class to test the hdllib class."

    def test_read_good_hdllib_file(self):
        lib = HdlLib("./cdf_dir/hdllib_files/test_hdllib.cfg")
        self.assertEqual(lib.hdl_lib_name, "technology")
        self.assertEqual(lib.hdl_library_clause_name, "technology_lib")
        self.assertEqual(lib.hdl_lib_technology, "")

    def test_read_wrong_hdllib_file(self):
        self.assertRaises(ConfigFileExceptionError, HdlLib, "./cdf_dir/hdllib_files/hdllib_wrong.cfg")


if __name__ == '__main__':
    unittest.main(verbosity=2)
