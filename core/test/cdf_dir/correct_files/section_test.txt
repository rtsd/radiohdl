# Testfile for testing sections the CommonDictFile class
#
# First some keys with different spacing (spaces and tabs)

global_key_1 = global_1
global_key_2 = [ "aap", "noot", "mies" ]  # this is seen as a section header instead of a value!!!

just_some_key_1 = global value1 which is lost without a warning
just_some_key_2 = global value2 which is lost without a warning

[section_1]
just_some_key_1 = section1 value1 which is lost without a warning
just_some_key_2 = section1 value2 which is lost without a warning

[section_2]
just_some_key_1 = section2 value1
just_some_key_2 = section2 value2

